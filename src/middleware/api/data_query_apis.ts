import {
  SaveQueryRequest,
  SaveQueryResponse,
  DataQuery,
  APIResponse,
  ResolveName,
} from '../../utils/types';
import { APIPrefix } from '../../utils/functions';

export const fetchDataQueries = async (
  project_id: number | undefined,
): Promise<DataQuery[]> => {
  try {
    const response = await fetch(
      APIPrefix() + '/v1/fetch_data_query?project=' + project_id,
      {
        method: 'GET',
        credentials: 'include',
      },
    );
    if (!response.ok) throw new Error('Failed to fetch data queries');

    const data: APIResponse<DataQuery> = await response.json();
    return data.data_queries;
  } catch (error) {
    console.error('Error fetching queries:', error);
    throw error;
  }
};

export const saveDataQuery = async (
  dataQuery: SaveQueryRequest,
): Promise<SaveQueryResponse> => {
  try {
    console.log('API URL: ' + APIPrefix());
    const response = await fetch(APIPrefix() + '/v1/add_data_query', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
      body: JSON.stringify(dataQuery),
    });

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(errorData.message || 'Failed to save data query');
    }

    return response.json();
  } catch (error) {
    console.error('Error saving data query:', error);
    throw error;
  }
};

export const ResolveSourceName = async (
  source_name: string | undefined,
): Promise<ResolveName> => {
  try {
    const response = await fetch(APIPrefix() + '/v1/resolve?source_name=' + source_name, {
      method: 'GET',
      credentials: 'include',
    });
    if (!response.ok) throw new Error('Failed to resolve source name: ' + source_name);

    const resolveResults = await response.json();
    return resolveResults;
  } catch (error) {
    console.error('Error resolving source name:', error);
    throw error;
  }
};

// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

if (process.env.REACT_APP_ENV != 'PROD') {
  const { server } = require('./mocks/server');

  // Mock Light/Dark mode
  Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: (query: string) => {
      if (query === '(prefers-color-scheme: dark)') {
        return {
          matches: true,
          media: query,
          onchange: null,
          addListener: jest.fn(),
          removeListener: jest.fn(),
          addEventListener: jest.fn(),
          removeEventListener: jest.fn(),
          dispatchEvent: jest.fn(),
        };
      }
      return {
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(),
        removeListener: jest.fn(),
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      };
    },
  });

  // Establish API mocking before any tests.
  beforeAll(async () => await server.listen());

  // Reset any runtime request handlers we may add during the tests
  afterEach(async () => await server.resetHandlers());

  // Cleanup after tests are done
  afterAll(async () => await server.close());
}

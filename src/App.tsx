import React, { Suspense, useState, useEffect } from 'react';
import './App.css';
import CSS from 'csstype';
import useLocalStorage from 'use-local-storage';
import './i18n/i18n';
import { useTranslation } from 'react-i18next';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// images
//import skaLogo from './icons/logo-SKA-small.png';
import skaoLogo from './icons/skao-logo.1270.png';
import srcLogo from './icons/srcnet-dark.svg';
import srcLightLogo from './icons/srcnet-light.svg';
//import tangerineLogo from './icons/tangerine-combined-scaled.png';
import folderIcon from './icons/folder.256.png';
import workflowIcon from './icons/workflow.512.png';
import jupyterIcon from './icons/jupyter.380.png';
import terminalIcon from './icons/terminal.256.png';
import homeIcon from './icons/home.614.png';
import magnifierIcon from './icons/magnifier.512.png';
import chartIcon from './icons/chart.512.png';
import monitorIcon from './icons/monitor.1600.png';
import planetIcon from './icons/planet.512.png';
import dishIcon from './icons/dish.512.png';
import galaxyIcon from './icons/galaxy.512.png';
//import homeWhiteIcon from './icons/home-white.512.png';
//import folderWhiteIcon from './icons/folder-white.512.png';
//import personWhiteIcon from './icons/person-white.512.png';
//import magnifierWhiteIcon from './icons/magnifier-white.512.png';
import lightdarkwhiteIcon from './icons/lightdark-white.png';
import lightdarkblackIcon from './icons/lightdark-black.png';
import gearsIcon from './icons/gears.gif';

// flag
import usukFlag from './icons/US-UK-flag.png';
import dutchFlag from './icons/Netherlands-flag.png';

// types
import {
  AccessToken,
  JobType,
  NotificationType,
  SiteStorageAreas,
  JupyterHUBs,
  ToolType,
} from './utils/types';
import { DataManagementPage } from './components/data-management/types';
import { DataCollectionPanelNotificationType } from './components/search-catalog/data-collection-panel';

// types relating to tasks.
import {
  TaskType,
  UserSettings,
  UserServiceToken,
  MoveDataToStorage,
} from './utils/tasks';
import { CurrentTask, DataCollection } from './utils/tasks';

// functions
import { APIPrefix, GetURL, hasSessionExpired } from './utils/functions';
import { StripDoubleQuotes } from './utils/functions';

// classes
import { SearchResultsType } from './components/search-catalog/search-catalog';
import Home from './components/home/home';
import SearchCatalog from './components/search-catalog/search-catalog';
import Tool from './tools/tool';
import UserControl from './components/user-control/user-control';
import ProjectButton from './components/project-panel/project-button';
import UserControlOIDCToken from './components/user-control/user-control-oidc-token';
import { HideDropdownMenu } from './utils/functions';
import SearchCompute from './components/search-compute/search-compute';
import DataManagement from './components/data-management/data-management';
import Visualisation from './components/visualisation/visualisation';
import UserManagement from './components/user-management/user-management';
import Preferences from './components/user-preferences/preferences';
import ViewNotifications from './components/user-control/view-notifications';
import SessionToast from './components/gateway-toast/session-toast';
import Scrollbox from './tools/scrollbox';
import {
  Event,
  EventType,
  EventTarget,
  InitiateComputeSearchParams,
  InitiateDMSearchParams,
  AddCollectionEvent,
} from './utils/events';

// LEDA 2605481

//	--------------------------------------------------------------------------
//
//	C O N S T A N T S
//
//	--------------------------------------------------------------------------

const TITLE = 'SRC-NET Prototype';

// language.
const LANGUAGE_OPTIONS: { value: string; label: string; icon: any }[] = [
  { value: 'en', label: 'English', icon: usukFlag },
  { value: 'nl', label: 'Nederlands', icon: dutchFlag },
];

//	--------------------------------------------------------------------------
//
//	T Y P E S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	H T M L   C U S T O M   C O M P O N E N T S
//
//	--------------------------------------------------------------------------

// ------------------------------------------------------------
//
//	This dropdown mimics a droplist listbox, but
//	allows an image to be displayed next to each list item.
//
// ------------------------------------------------------------

function LanguageDropdownMenu(args: {
  sLanguageDropdownMenuDisplayed: boolean;
  setLanguageDropdownMenuDisplayed: any;
  buttonHandler: any;
}) {
  // function that monitors for mouse clicks. we need to add {ref} to the DIV element of the dropdown menu.
  const { ref } = HideDropdownMenu({
    sDropdownDisplayed: args.sLanguageDropdownMenuDisplayed,
    setDropdownDisplayed: args.setLanguageDropdownMenuDisplayed,
  });

  //	------------------------------------------------------------
  //
  //	A HTML component that renders a single menu item on the
  //	dropdown menu.
  //
  //	------------------------------------------------------------

  function MenuItem(args: { name: string; text: string; icon?: string; onClick: any }) {
    return (
      <button
        name={'btnLanguage_' + args.name}
        className="dropdown-list-item"
        onClick={args.onClick}
      >
        <div className="dropdown-list-item-text">{args.text}</div>
        <div className="dropdown-list-item-image">
          <img src={args.icon} alt="" width="30" height="16" />
        </div>
      </button>
    );
  } // MenuItem

  return (
    <div
      ref={ref}
      style={{
        display: args.sLanguageDropdownMenuDisplayed === true ? 'block' : 'none',
        position: 'absolute',
        top: '100%',
        right: '0%',
        border: 'none',
        zIndex: '9',
      }}
    >
      <div className="language-dropdown-menu">
        {LANGUAGE_OPTIONS.map((item, index) => (
          <MenuItem
            key={index}
            name={item.value}
            text={item.label}
            icon={item.icon}
            onClick={args.buttonHandler}
          />
        ))}
      </div>
    </div>
  );
} // LanguageDropdownMenu

// ------------------------------------------------------------
//
//	Small icons that appear at the top of the screen to
//	allow the user to quickly navigate the portal.
//
// ------------------------------------------------------------

function NavigationIcon(args: {
  icon: string;
  tooltip: string;
  name: string;
  onClick: any;
}) {
  return (
    <button
      className="navigation-icon"
      name={args.name}
      onClick={args.onClick}
      title={args.tooltip}
    >
      <img src={args.icon} alt="" width="24" height="24" />
    </button>
  );
} // NavigationIcon

//	--------------------------------------------------------------------------
//
//	M A I N   A P P L I C A T I O N
//
//	--------------------------------------------------------------------------

function App() {
  // multi-language support
  const { i18n, t } = useTranslation();

  // get default user preferences..
  const DEFAULT_DARK = window.matchMedia('(prefers-color-scheme: dark)').matches;
  const DEFAULT_LANGUAGE = getBrowserDefaultLanguage({ defaultLang: 'en' });

  // define an empty token.
  const EMPTY_TOKEN: AccessToken = {
    access_token: '',
    token_type: '',
    refresh_token: '',
    expires_in: 0,
    scope: '',
    id_token: '',
    expires_at: 0,
  };

  // define the toolset available along the top of the page.
  const TOOLS: {
    name: string;
    icon: any;
    text: string;
    toolType: ToolType;
    needSiteCapabilities?: boolean;
    needDataManagement?: boolean;
    needLoggedIn?: boolean;
    needJupyterURL?: boolean;
  }[] = [
    {
      name: 'btnHome',
      icon: { homeIcon },
      text: 'Home',
      toolType: ToolType.HOME,
    },
    {
      name: 'btnSearchCatalog',
      icon: { magnifierIcon },
      text: 'Search catalogue',
      toolType: ToolType.SEARCH_CATALOG,
    },
    {
      name: 'btnSearchCompute',
      icon: { monitorIcon },
      text: 'Search compute resources',
      toolType: ToolType.SEARCH_COMPUTE,
      needSiteCapabilities: true,
      needLoggedIn: true,
    },
    {
      name: 'btnDataManagement',
      icon: { monitorIcon },
      text: 'Data management',
      toolType: ToolType.DATA_MANAGEMENT,
      needDataManagement: true,
      needLoggedIn: true,
    },
    {
      name: 'btnNotebook',
      icon: { jupyterIcon },
      text: 'Notebook',
      toolType: ToolType.NOTEBOOK,
      needJupyterURL: true,
    },
    {
      name: 'btnVisualiseData',
      icon: { chartIcon },
      text: 'Visualise data',
      toolType: ToolType.VISUALISE_DATA,
    } /*,
					{
					name: 'btnUserManagement',
					icon: undefined,
					text: 'User management',
					toolType: ToolType.USER_MANAGEMENT,
					needLoggedIn: true
					},
					{
					name: 'btnWorkflow',
					icon: {workflowIcon},
					text: 'Workflow',
					toolType: ToolType.WORKFLOW
					},
					{
					name: 'btnMyFiles',
					icon: {folderIcon},
					text: 'Files',
					toolType: ToolType.MY_FILES
					},
					{
					name: 'btnTerminal',
					icon: {terminalIcon},
					text: 'Terminal',
					toolType: ToolType.TERMINAL
					},
					{
					name: 'btnVMs',
					icon: {monitorIcon},
					text: 'VMs',
					toolType: ToolType.VIRTUAL_MACHINE
					}*/,
  ];

  // set a page title using a hook.
  useEffect(() => {
    document.title = TITLE;
  }, []);

  // if we've been returned from logging into IAM then get the authorisation code.
  const queryParameters = new URLSearchParams(window.location.search);
  const authorisationCode = queryParameters.get('code');

  //	--------------------------------------------------------------------------
  //
  //	D E C L A R A T I O N S
  //
  //	--------------------------------------------------------------------------

  // general:
  const [sSelectedProject, setSelectedProject] = useState<number>(0);
  const [sSelectedTool, setSelectedTool] = useState<ToolType>(ToolType.HOME);

  // re-direct uri.
  const [sRedirectURI, setRedirectURI] = useState<string>('');

  // language:
  const [sLanguageDropdownMenuDisplayed, setLanguageDropdownMenuDisplayed] =
    useState<boolean>(false);
  const [sLanguage, setLanguage] = useLocalStorage('gateway_language', DEFAULT_LANGUAGE);
  const [sLanguageIndex, setLanguageIndex] = useState<number>(
    findLanguageIndex({
      language: StripDoubleQuotes({ value: localStorage.getItem('gateway_language') }),
    }),
  );

  // light/dark mode theme.
  const [sTheme, setTheme] = useLocalStorage(
    'gateway_theme',
    DEFAULT_DARK ? 'dark' : 'light',
  );

  // user control:
  const [sUserDropdownMenuDisplayed, setUserDropdownMenuDisplayed] =
    useState<boolean>(false);
  const [sUsername, setUsername] = useState<string>(
    authorisationCode === null ? i18n.t('Unknown User') : i18n.t('Logging in') + '...',
  );
  const [sPreferredUsername, setPreferredUsername] = useState<string>('');

  // notebook tool:
  const [sJupyterBaseURL, setJupyterBaseURL] = useState<string>('');
  const [sJupyterURL, setJupyterURL] = useState<string>('');
  const [sJupyterRender, setJupyterRender] = useState<number>(0);
  const [xsrfToken, setXsrfToken] = useState<string | null>(null);
  const [gatewayToken, setgatewayToken] = useState<string | null>(null);

  // access token for OIDC.
  const [sLoggedIn, setLoggedIn] = useState<boolean | undefined>(undefined);
  const [sSessionValid, setSessionValid] = useState<boolean>(false);

  // Use to trigger component reloads on successful logins
  const [sLoginCount, setLoginCount] = useState<number>(0);

  // loading tokens
  const [sLoadingToken, setLoadingToken] = useState<boolean>(false);

  // tokens obtained
  const [sSiteCapabilitiesTokenObtained, setSiteCapabilitiesTokenObtained] =
    useState<boolean>(false);
  const [sDataManagementTokenObtained, setDataManagementTokenObtained] =
    useState<boolean>(false);

  // Dialog boxes
  const [sShowPreferencesDialog, setShowPreferencesDialog] = useState<boolean>(false);

  // data-management jobs.
  const [sDataManagementJobs, setDataManagementJobs] = useState<JobType[]>([]);
  const [sJobsLoading, setJobsLoading] = useState<boolean>(false);

  // notifications and render count.
  const [sNotifications, setNotifications] = useState<NotificationType[]>([]);

  // storage areas.
  const [sStorageAreas, setStorageAreas] = useState<SiteStorageAreas[]>([]);

  // local JupyterHUBs for each site.
  const [sJupyterHUBs, setJupyterHUBs] = useState<JupyterHUBs[]>([]);

  // list of projects the user is a member of.
  const [sProjects, setProjects] = useState<
    {
      id: number;
      name: string;
      description: string;
      abbreviation: string;
      icon: string;
    }[]
  >([]);
  const [sLoadingProjects, setLoadingProjects] = useState<boolean>(false);

  // login trigger
  const [sLoginTrigger, setLoginTrigger] = useState<{
    code: string | null;
    redirectURI: string;
  }>({ code: null, redirectURI: '' });

  //	--------------------------------------------------------------------------
  //
  //	E V E N T   D E C L A R A T I O N S
  //
  //	--------------------------------------------------------------------------

  // data-management tab.
  const [sEventToDataManagement, setEventToDataManagement] = useState<any>(undefined);
  const eventToDataManagement = (newFunction: any) => {
    setEventToDataManagement(() => newFunction);
  }; // eventToDataManagement

  // search-catalogue tab.
  const [sEventToSearchCatalogue, setEventToSearchCatalogue] = useState<any>(undefined);
  const eventToSearchCatalogue = (newFunction: any) => {
    setEventToSearchCatalogue(() => newFunction);
  }; // eventToSearchCatalogue

  // search-compute tab.
  const [sEventToSearchCompute, setEventToSearchCompute] = useState<any>(undefined);
  const eventToSearchCompute = (newFunction: any) => {
    setEventToSearchCompute(() => newFunction);
  }; // eventToSearchCompute

  // user control.
  const [sEventToUserControl, setEventToUserControl] = useState<any>(undefined);
  const eventToUserControl = (newFunction: any) => {
    setEventToUserControl(() => newFunction);
  }; // eventToUserControl

  // view notifications.
  const [sEventToViewNotifications, setEventToViewNotifications] =
    useState<any>(undefined);
  const eventToViewNotifications = (newFunction: any) => {
    setEventToViewNotifications(() => newFunction);
  }; // eventToViewNotifications

  //	--------------------------------------------------------------------------
  //
  //	F U N C T I O N S
  //
  //	--------------------------------------------------------------------------

  //	------------------------------------------------------------
  //
  //	Add to an existing collection in the database.
  //
  //	------------------------------------------------------------

  async function addToExistingDataCollection(args: { dataCollection: DataCollection }) {
    console.log('addToExistingDataCollection fires:');
    console.log(args.dataCollection);
    if (args.dataCollection.dataCollection !== undefined)
      if (args.dataCollection.dataCollection.id !== undefined) {
        var urlCommand: string =
          APIPrefix() +
          '/v1/add_to_data_collection' +
          '?data_collection_id=' +
          (args.dataCollection.dataCollection.id !== undefined
            ? args.dataCollection.dataCollection.id.toString()
            : '-1');

        try {
          const apiResult = await fetch(urlCommand, {
            method: 'PUT',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            credentials: 'include',
            body:
              args.dataCollection.dids !== undefined
                ? JSON.stringify(args.dataCollection.dids)
                : JSON.stringify([]),
          });

          const returnedJson = await apiResult.json();

          // HTTP 201 means the items have been added to the collection.
          if (apiResult.status === 201) {
            // get the number of items added.
            var itemsAdded: number = 0;
            try {
              itemsAdded = returnedJson.additional_info.items_added;
            } catch (e) {}

            // Display confirmation message
            const params: AddCollectionEvent = {
              collectionName:
                args.dataCollection.dataCollection !== undefined
                  ? args.dataCollection.dataCollection.name
                  : '',
              itemsAdded: itemsAdded,
              errorTarget:
                args.dataCollection.messageSearchCatalog === true
                  ? EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
                  : undefined,
            };
            const newEvent: Event = {
              eventTarget: EventTarget.GLOBAL,
              eventType: EventType.ITEMS_ADDED_TO_DATA_COLLECTION,
              parameters: params,
            };
            raiseEvent({ event: newEvent });
          }

          // HTTP 401 means the session has expired.
          else if (apiResult.status === 401) {
            console.log('addToExistingDataCollection 401.');
          }

          // HTTP 403 means the user is not a member of this project.
          else if (apiResult.status === 403) {
            const params: AddCollectionEvent = {
              projectName:
                args.dataCollection.project !== undefined
                  ? args.dataCollection.project.name
                  : '',
              errorTarget:
                args.dataCollection.messageSearchCatalog === true
                  ? EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
                  : undefined,
            };
            const newEvent: Event = {
              eventTarget: EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL,
              eventType: EventType.USER_NOT_PROJECT_MEMBER_EXISTING_COLLECTION,
              parameters: params,
            };
            raiseEvent({ event: newEvent });
          }

          // HTTP 404 means a data collection with this id does not exist.
          else if (apiResult.status === 404) {
            const params: AddCollectionEvent = {
              collectionName:
                args.dataCollection.dataCollection !== undefined
                  ? args.dataCollection.dataCollection.name
                  : '',
              errorTarget:
                args.dataCollection.messageSearchCatalog === true
                  ? EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
                  : undefined,
            };
            const newEvent: Event = {
              eventTarget: EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL,
              eventType: EventType.DATA_COLLECTION_DOES_NOT_EXIST,
              parameters: params,
            };
            raiseEvent({ event: newEvent });
          }
          // anything else, then display an error message.
          else {
            // TODO: Display error message
          }
        } catch (e) {}
      }
  } // addToExistingDataCollection

  //	-------------------------------------------------
  //
  //	Build a list of parameters for the visualisation
  //	tools.
  //
  //	The parameter list is an array that consists of:
  //
  //	1. a code, i.e. 'aladin'
  //	2. a set of launch parameters. if these parameters are not null then the
  //		tool will automatically be launched without user intervention.
  //	3. a set of other parameters required by the tool.
  //
  //	-------------------------------------------------

  function buildVisualisationParams() {
    //	-------------------------------------------------
    //
    //	parameters for Aladin.
    //
    //	-------------------------------------------------

    var aladinLaunchParams: {
      position: {
        ra: number;
        dec: number;
        obs_publisher_did: string;
      } | null;
    } | null = null;
    const aladinParams: {
      updateState: any;
    } | null = {
      updateState: updateState,
    };

    //	-------------------------------------------------
    //
    //	parameters for SkyServer.
    //
    //	-------------------------------------------------

    const skyserverParams: {
      updateState: any;
    } | null = {
      updateState: updateState,
    };

    //	-------------------------------------------------
    //
    //	combine the parameters into a single data structure.
    //
    //	-------------------------------------------------

    const params: {
      code: string;
      launchParams: {} | null;
      params: {} | null;
    }[] = [
      {
        code: 'aladin',
        launchParams: aladinLaunchParams,
        params: aladinParams,
      },
      {
        code: 'skyserver',
        launchParams: null,
        params: skyserverParams,
      },
    ];

    // return something.
    return params;
  } // buildVisualisationParams

  //	------------------------------------------------------------
  //
  //	Create a new collection in the database.
  //
  //	------------------------------------------------------------

  async function createNewDataCollection(args: { dataCollection: DataCollection }) {
    console.log('createNewDataCollection fires:');
    console.log(args.dataCollection);
    if (args.dataCollection.dataCollection !== undefined)
      if (
        args.dataCollection.dataCollection.name !== undefined &&
        args.dataCollection.dataCollection.name !== ''
      ) {
        var urlCommand: string =
          APIPrefix() +
          '/v1/add_data_collection' +
          '?project_id=' +
          (args.dataCollection.project !== undefined
            ? args.dataCollection.project.id.toString()
            : '-1') +
          '&name=' +
          args.dataCollection.dataCollection.name;

        try {
          const apiResult = await fetch(urlCommand, {
            method: 'PUT',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            credentials: 'include',
            body:
              args.dataCollection.dids !== undefined
                ? JSON.stringify(args.dataCollection.dids)
                : JSON.stringify([]),
          });

          const returnedJson = await apiResult.json();

          // HTTP 201 means the collection has been added.
          if (apiResult.status === 201) {
            // get the number of items added.
            var itemsAdded: number = 0;
            try {
              itemsAdded = returnedJson.additional_info.items_added;
            } catch (e) {}

            // Display confirmation message
            const params: AddCollectionEvent = {
              collectionName:
                args.dataCollection.dataCollection !== undefined
                  ? args.dataCollection.dataCollection.name
                  : '',
              itemsAdded: itemsAdded,
              errorTarget:
                args.dataCollection.messageSearchCatalog === true
                  ? EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
                  : undefined,
            };
            const newEvent: Event = {
              eventTarget: EventTarget.GLOBAL,
              eventType: EventType.DATA_COLLECTION_ADDED,
              parameters: params,
            };
            raiseEvent({ event: newEvent });
          }

          // HTTP 400 means a data collection with this name already exists.
          else if (apiResult.status === 400) {
            // Display confirmation message
            const params: AddCollectionEvent = {
              collectionName:
                args.dataCollection.dataCollection !== undefined
                  ? args.dataCollection.dataCollection.name
                  : '',
              errorTarget:
                args.dataCollection.messageSearchCatalog === true
                  ? EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
                  : undefined,
            };
            const newEvent: Event = {
              eventTarget: EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL,
              eventType: EventType.DATA_COLLECTION_ALREADY_EXISTS,
              parameters: params,
            };
            raiseEvent({ event: newEvent });
          }

          // HTTP 401 means the session has expired.
          else if (apiResult.status === 401) {
            console.log('createNewDataCollection 401.');
          }

          // HTTP 403 means the user is not a member of this project.
          else if (apiResult.status === 403) {
            // TODO: Display error message
          }

          // anything else, then display an error message.
          else {
            // TODO: Display error message
          }
        } catch (e) {}
      }
  } // createNewDataCollection

  //	------------------------------------------------------------
  //
  //	Delete a data collection from the database.
  //
  //	------------------------------------------------------------

  async function deleteDataCollection(args: { dataCollection: DataCollection }) {
    console.log('deleteDataCollection fires:');
    console.log(args.dataCollection);
    if (args.dataCollection.collectionIDs !== undefined)
      if (args.dataCollection.collectionIDs.length > 0) {
        var urlCommand: string = APIPrefix() + '/v1/delete_data_collections';

        try {
          const apiResult = await fetch(urlCommand, {
            method: 'PUT',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            credentials: 'include',
            body:
              args.dataCollection.collectionIDs !== undefined
                ? JSON.stringify(args.dataCollection.collectionIDs)
                : JSON.stringify([]),
          });

          const returnedJson = await apiResult.json();

          // HTTP 200 means the data collection has been deleted.
          if (apiResult.status === 200) {
            // raise a deleted-collection event.
            const newEvent: Event = {
              eventTarget: EventTarget.GLOBAL,
              eventType: EventType.DATA_COLLECTION_DELETED,
            };
            raiseEvent({ event: newEvent });
          }

          // HTTP 401 means the session has expired.
          else if (apiResult.status === 401) {
            console.log('deleteDataCollection 401.');
          }

          // HTTP 403 means the user is not a member of this project.
          /*else if (apiResult.status === 403) {
        }*/

          // HTTP 404 means a data collection with this id does not exist.
          /* else if (apiResult.status === 404) {
        // HTTP 403 means the user is not a member of this project.
        /*else if (apiResult.status === 403) {
        }*/

          // HTTP 404 means a data collection with this id does not exist.
          /* else if (apiResult.status === 404) {
        }*/

          // anything else, then display an error message.
          /*else {
            // TODO: Display error message
          }*/
        } catch (e) {}
      }
  } // deleteDataCollection

  //	-------------------------------------------------
  //
  //	delete a list of notifications, and raise a
  //	global event to refresh notifications.
  //
  //	-------------------------------------------------

  function deleteNotifications(args: { notificationIDs: number[] }) {
    // remove the notification from the list.
    var notifications: NotificationType[] = sNotifications.slice();
    for (var i: number = 0; i < args.notificationIDs.length; i++) {
      const notificationPos: number = notifications.findIndex(
        (item) => item.notificationID === args.notificationIDs[i],
      );
      if (notificationPos > -1) notifications.splice(notificationPos, 1);
    }
    setNotifications(notifications);

    // raise a global refresh notifications event.
    const newEvent: Event = {
      eventTarget: EventTarget.GLOBAL,
      eventType: EventType.REFRESH_NOTIFICATIONS,
    };
    raiseEvent({ event: newEvent });
  } // deleteNotifications

  //	-------------------------------------------------
  //
  //	delete a list of notifications from the DB.
  //
  //	-------------------------------------------------

  async function deleteNotificationsFromDB(args: { notificationIDs: number[] }) {
    console.log('deleteNotificationsFromDB fires:');
    console.log(args.notificationIDs);
    try {
      // call the /delete_notification end point here.
      const apiResult = await fetch(APIPrefix() + '/v1/delete_notification', {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify(args.notificationIDs),
      });

      console.log(apiResult);

      // HTTP 401 means the session has expired.
      if (apiResult.status === 401) {
        console.log('deleteNotifications 401.');
      }
    } catch (e) {
      console.log('error:');
      console.log(e);
    }
  } // deleteNotificationsFromDB

  //	-------------------------------------------------
  //
  //	Finds the index of the user's language in the
  //	language dropdown box.
  //
  //	-------------------------------------------------

  function findLanguageIndex(args: { language: string | null }) {
    var languageIndex: number = 0;
    if (args.language !== null) {
      // get the language index.
      var languageIndex: number = LANGUAGE_OPTIONS.findIndex(
        (element) => element.value === args.language,
      );
      if (languageIndex === -1) languageIndex = 0;
    }

    // return something.
    return languageIndex;
  } // findLanguageIndex

  //	-------------------------------------------------
  //
  //	 Calls the /login endpoint.
  //
  //	-------------------------------------------------

  async function userLogin(args: { code: string | null; redirectURI: string | null }) {
    let query_params = '';

    if (args.code !== null)
      query_params = '?code=' + args.code + '&redirect_uri=' + args.redirectURI;

    setLoadingToken(true);

    try {
      const apiResult = await fetch(APIPrefix() + '/v1/login' + query_params, {
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
      });

      if (apiResult.status === 200) {
        const returnedJson = await apiResult.json();

        if (returnedJson.logged_in === true) {
          if (returnedJson.name !== undefined) setUsername(returnedJson.name);

          if (returnedJson.preferred_username !== undefined)
            setPreferredUsername(returnedJson.preferred_username);

          if (returnedJson.gateway_backend_token !== undefined)
            setgatewayToken(returnedJson.gateway_backend_token);

          if (returnedJson.token_info !== undefined) {
            if (returnedJson.token_info.data_management_api !== undefined) {
              setDataManagementTokenObtained(
                returnedJson.token_info.data_management_api.obtained,
              );
            }
            if (returnedJson.token_info.site_capabilities_api !== undefined) {
              setSiteCapabilitiesTokenObtained(
                returnedJson.token_info.site_capabilities_api.obtained,
              );
            }
          }

          setSessionValid(true);
          setLoggedIn(true);

          loadProjects();
          loadUserSettings();
          getDataManagementJobs();
        } else {
          setDataManagementTokenObtained(false);
          setSiteCapabilitiesTokenObtained(false);
          setSessionValid(false);
          setLoggedIn(false);
        }
      } else {
        setDataManagementTokenObtained(false);
        setSiteCapabilitiesTokenObtained(false);
        setSessionValid(false);
        setLoggedIn(false);
      }
    } catch (e) {
      console.log(e);
      setDataManagementTokenObtained(false);
      setSiteCapabilitiesTokenObtained(false);
      setSessionValid(false);
      setLoggedIn(false);
    }

    setLoginCount((prevCount: number) => prevCount + 1);
    setLoadingToken(false);
  }

  //	-------------------------------------------------
  //
  //	Gets the user's language from the browser
  //	settings. If the language is hyphenated, i.e. en-US,
  //	then we will only use the bit from before the hyphen.
  //
  //	-------------------------------------------------

  function getBrowserDefaultLanguage(args: { defaultLang: string }) {
    // get the browser's language.
    var browserLanguage = navigator.language;

    var language: string = args.defaultLang;
    if (browserLanguage !== '') {
      // extract the characters before the first '-'.
      const hyphenPos = browserLanguage.indexOf('-');
      if (hyphenPos > -1) language = browserLanguage.slice(0, hyphenPos);
      else language = browserLanguage;
    }

    // return something.
    return language;
  } // getBrowserDefaultLanguage

  //	------------------------------------------------------------
  //
  //	An asynchronous function that loads a list of data-management
  //	API jobs.
  //
  //	------------------------------------------------------------

  async function getDataManagementJobs() {
    setJobsLoading(true);

    try {
      var urlCommand: string = APIPrefix() + '/v1/data_management/list_jobs';

      try {
        const apiResult = await fetch(urlCommand, {
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        });

        if (apiResult.status === 200) {
          const returnedJson = await apiResult.json();

          // get jobs list.
          var jobsList: JobType[] = [];
          if (returnedJson.jobs_list !== undefined) jobsList = returnedJson.jobs_list;

          // remove jobs with status 'NOT FOUND'.
          for (var i = jobsList.length - 1; i >= 0; i--)
            if (jobsList[i].status === 'NOT FOUND') jobsList.splice(i, 1);

          // update the state with the list of returned jobs.
          setDataManagementJobs(jobsList);
        }

        // HTTP 401 means the session has expired.
        if (apiResult.status === 401) {
          console.log('getDataManagementJobs 401.');
        }
      } catch (e) {
        setDataManagementJobs([]);
        console.log(e);
      }
    } catch (e) {
      setDataManagementJobs([]);
      console.log(e);
    }

    // clear loading boolean.
    setJobsLoading(false);
  } // getDataManagementJobs

  //	-------------------------------------------------
  //
  //	launch a Jupyter notebook when the user selects one from
  //	the compute-resource search
  //
  //	-------------------------------------------------
  const fetchXsrfToken = async (url: string) => {
    console.log('fetchXsrfToken fires');
    try {
      const response = await fetch(
        url + (url.slice(-1) === '/' ? '' : '/') + 'hub/login',
        {
          credentials: 'include',
        },
      );

      if (response.ok) {
        const htmlText = await response.text();
        //console.log("HTML " + htmlText);
        const parser = new DOMParser();
        const doc = parser.parseFromString(htmlText, 'text/html');
        const xsrfInput = doc.querySelector('input[name="_xsrf"]') as HTMLInputElement;

        console.log('XSRF ' + xsrfInput.value);
        if (xsrfInput) {
          console.log('setting XSRF token');
          setXsrfToken(xsrfInput.value);
        }
      }
    } catch (error) {
      console.error('Failed to fetch XSRF token:', error);
    }
  };

  const loginUserJupyterHub = async (url: string) => {
    try {
      const formData = new FormData();
      console.log('loginUserJupyterHub fires');
      if (gatewayToken) {
        formData.append('token', gatewayToken || '');
        formData.append('_xsrf', xsrfToken || '');

        // clear the XSRF token.
        setXsrfToken(null);

        const response = await fetch(
          url + (url.slice(-1) === '/' ? '' : '/') + 'hub/login',
          {
            method: 'POST',
            credentials: 'include',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: new URLSearchParams(formData as any).toString(),
          },
        );

        if (response.ok) {
          setJupyterURL(url + (url.slice(-1) === '/' ? '' : '/') + 'hub/spawn');
          var jupyterRender: number = sJupyterRender;
          setJupyterRender(Date.now());
          setSelectedTool(ToolType.NOTEBOOK);
        } else {
          console.error('Login Failed :(', response.statusText);
        }
      }
    } catch (error) {
      console.error('Error occurred while Login :(', error);
    }
  };

  const launchNotebook = async (args: { url: string }) => {
    try {
      // update the url and the render count (we need this to force a re-render when the user clicks 'launch' again).
      if (args.url !== undefined) {
        //TODO: Firstly we need clear any tokens stored for JHub. Assuming the user launchd the notebook before
        console.log('launch notebook: ' + args.url);
        console.log('preferred username: ' + sPreferredUsername);
        if (sPreferredUsername) {
          //const jupyter_username = JSON.parse(session_username);
          const response = await fetch(
            args.url +
              (args.url.slice(-1) === '/' ? '' : '/') +
              'user/' +
              sPreferredUsername +
              '/logout',
            {
              credentials: 'include',
            },
          );
          console.log('launchNotebook: logout response received');
          console.log(args.url);
          console.log(response);
          if (response.ok) {
            // In order to re-SSO the user to JupyterHubs, we need to extract the _xsrf token
            setJupyterBaseURL(args.url);
            console.log('launchNotebook: fetching XSRF token');
            await fetchXsrfToken(args.url);
          }
        }
      }
    } catch (error) {
      console.error('Error occurred while launching notebook :(', error);
    }
  }; // launchNotebook

  useEffect(() => {
    // Submit the form when xsrfToken and access token is set
    console.log('GW Token: ' + gatewayToken);
    console.log('XSRF Token: ' + xsrfToken);
    if (xsrfToken && gatewayToken) {
      loginUserJupyterHub(sJupyterBaseURL);
    }
  }, [xsrfToken, gatewayToken]);

  //	-------------------------------------------------
  //
  //	Convert an image into a base64 string for
  //	storing in the database.
  //
  //	-------------------------------------------------

  async function imageUrlToBase64(url: any) {
    let string = '';
    const response = await fetch(url);
    const buffer = await response.arrayBuffer();
    new Uint8Array(buffer).forEach((byte) => {
      string += String.fromCharCode(byte);
    });
    console.log(btoa(string));

    return btoa(string);
  } // imageUrlToBase64

  //	------------------------------------------------------------
  //
  //	An asynchronous function that loads a list of local
  //	JupyterHUB services from the site-capabilities API.
  //
  //	------------------------------------------------------------

  async function loadJupyterHUBs() {
    try {
      var urlCommand: string =
        APIPrefix() + '/v1/site_capabilities/list_compute?service_type=jupyterhub';

      try {
        const apiResult = await fetch(urlCommand, {
          headers: { 'Content-Type': 'application/json' },
          credentials: 'include',
        });
        if (apiResult.status === 200) {
          // get jupyter hubs.
          var compute: object[] = [];
          var jupyterHUBs: JupyterHUBs[] = [];

          const returnedJson = await apiResult.json();
          if (returnedJson.compute !== undefined) compute = returnedJson.compute;

          // extract the information we need.
          for (var i = 0; i < compute.length; i++) {
            var site: any = compute[i];

            // check if this site already exists.
            const index = jupyterHUBs.findIndex((element) => element.site === site.site);

            if ('associated_services' in site)
              if (site['associated_services'].length > 0) {
                var associated_services: {
                  id: string;
                  prefix: string;
                  host: string;
                  path: string;
                  identifier: string;
                  port: number;
                }[] = [];
                if (index > -1)
                  associated_services = jupyterHUBs[index].associated_services;
                for (var j = 0; j < site.associated_services.length; j++) {
                  var associated_service: {
                    id: string;
                    prefix: string;
                    host: string;
                    path: string;
                    identifier: string;
                    port: number;
                  } = {
                    id: site.associated_services[j].id,
                    prefix: site.associated_services[j].prefix,
                    host: site.associated_services[j].host,
                    path: site.associated_services[j].path,
                    identifier: site.associated_services[j].identifier,
                    port: site.associated_services[j].port,
                  };
                  associated_services.push(associated_service);
                }
                if (index === -1) {
                  var jupyterHUB: JupyterHUBs = {
                    site: site.site,
                    associated_services: associated_services,
                  };
                  jupyterHUBs.push(jupyterHUB);
                }
              }
          }

          // update the state with the list of returned JupyterHUBs.
          setJupyterHUBs(jupyterHUBs);
        }

        // HTTP 401 means the session has expired.
        if (apiResult.status === 401) {
          console.log('Failed to load JupyterHUBs.');
        }
      } catch (e) {
        console.log(e);
      }
    } catch (e) {
      console.log(e);
    }
  } // loadJupyterHUBs

  //	-------------------------------------------------
  //
  //	load the list of projects that this user is
  //	a member of.
  //
  //	-------------------------------------------------

  async function loadProjects() {
    setLoadingProjects(true);
    console.log('loadProjects() fires');
    try {
      // call the /get_preferences endpoint here.
      const apiResult = await fetch(APIPrefix() + '/v1/get_projects', {
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
      });
      if (apiResult.status === 200) {
        const returnedJson = await apiResult.json();

        if (returnedJson.projects !== undefined) {
          var projects: {
            id: number;
            name: string;
            description: string;
            abbreviation: string;
            icon: string;
          }[] = [];
          for (var i: number = 0; i < returnedJson.projects.length; i++) {
            var newProject: {
              id: number;
              name: string;
              description: string;
              abbreviation: string;
              icon: string;
            } = {
              id: returnedJson.projects[i].project_id,
              name: returnedJson.projects[i].name,
              description: returnedJson.projects[i].description,
              abbreviation: returnedJson.projects[i].abbreviation,
              icon: returnedJson.projects[i].icon,
            };
            projects.push(newProject);
          }

          // set state.
          setProjects(projects);
        }
      }
    } catch (e) {}
    setLoadingProjects(false);
  } // loadProjects

  //	------------------------------------------------------------
  //
  //	An asynchronous function that loads a list of storage
  //	areas from the site-capabilities API.
  //
  //	------------------------------------------------------------

  async function loadStorageAreas() {
    try {
      var urlCommand: string = APIPrefix() + '/v1/site_capabilities/list_storage';

      try {
        const apiResult = await fetch(urlCommand, {
          headers: { 'Content-Type': 'application/json' },
          credentials: 'include',
        });
        if (apiResult.status === 200) {
          // get storage list.
          var storageList: SiteStorageAreas[] = [];

          const returnedJson = await apiResult.json();
          if (returnedJson.storage !== undefined) storageList = returnedJson.storage;

          // remove any sites that do not have storage areas.
          for (var i = storageList.length - 1; i >= 0; i--)
            if (storageList[i].storage_areas.length === 0) storageList.splice(i, 1);

          // update the state with the list of returned storage areas.
          setStorageAreas(storageList);
        }

        // HTTP 401 means the session has expired.
        if (apiResult.status === 401) {
          console.log('Failed to load storage areas.');
        }
      } catch (e) {
        console.log(e);
      }
    } catch (e) {
      console.log(e);
    }
  } // loadStorageAreas

  //	-------------------------------------------------
  //
  //	mark a list of notifications as READ.
  //
  //	-------------------------------------------------

  async function markNotificationsRead(args: { notificationIDs: number[] }) {
    // update the notifications to mark them as read.
    var notifications: NotificationType[] = sNotifications.slice();
    for (var i: number = 0; i < args.notificationIDs.length; i++) {
      const notificationPos: number = notifications.findIndex(
        (item) => item.notificationID === args.notificationIDs[i],
      );
      if (notificationPos > -1) notifications[notificationPos].readFlag = true;
    }
    setNotifications(notifications);

    // raise a global refresh notifications event.
    const newEvent: Event = {
      eventTarget: EventTarget.GLOBAL,
      eventType: EventType.REFRESH_NOTIFICATIONS,
    };
    raiseEvent({ event: newEvent });
  } // markNotificationsRead

  //	-------------------------------------------------
  //
  //	mark a list of notifications as READ in the DB.
  //
  //	-------------------------------------------------

  async function markNotificationsReadInDB(args: { notificationIDs: number[] }) {
    console.log('markNotificationsReadinDB fires:');
    console.log(args.notificationIDs);
    try {
      // call the /mark_notifications_read end point here.
      const apiResult = await fetch(APIPrefix() + '/v1/mark_notifications_read', {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify(args.notificationIDs),
      });

      console.log(apiResult);

      // HTTP 401 means the session has expired.
      if (apiResult.status === 401) {
        console.log('markNotificationsRead 401.');
      }
    } catch (e) {
      console.log('error:');
      console.log(e);
    }
  } // markNotificationsReadInDB

  //	-------------------------------------------------
  //
  //	retrieve backend token
  //
  //	-------------------------------------------------
  async function retrieveBackendToken() {
    console.log('retrieveBackendToken() fires');
    try {
      // call the /get_tokens endpoint here.
      const apiResult = await fetch(APIPrefix() + '/v1/get_tokens', {
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
      });

      if (apiResult.status === 200) {
        const returnedJson = await apiResult.json();

        // set backend_gateway_token
        setgatewayToken(returnedJson.gateway_backend_token);
      }
    } catch (e) {}
  } // retrieveBackendToken

  //	-------------------------------------------------
  //
  //	load and apply the user settings
  //
  //	-------------------------------------------------

  async function loadUserSettings() {
    console.log('loadUserSettings() fires');
    try {
      // call the /get_preferences endpoint here.
      const apiResult = await fetch(APIPrefix() + '/v1/get_preferences', {
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
      });
      if (apiResult.status === 200) {
        const returnedJson = await apiResult.json();

        // store the theme and language.
        setTheme(returnedJson.darkMode === true ? 'dark' : 'light');

        if (returnedJson.language !== '') {
          // change the language.
          //i18n.changeLanguage( returnedJson.language );
          setLanguage(returnedJson.language);

          // update the language index.
          var languageIndex: number = findLanguageIndex({
            language: returnedJson.language,
          });
          setLanguageIndex(languageIndex);
        }
      }

      // HTTP 401 means the session has expired.
      if (apiResult.status === 401) {
        console.log('loadUserSettings 401.');
      }
    } catch (e) {}
  } // loadUserSettings

  //	------------------------------------------------------------
  //
  //	Call the API to log out by removing the session cookies
  //
  //	------------------------------------------------------------

  async function logout() {
    // remove cookies, and update state variables.
    console.log('setLoggedIn false (logout)');
    setDataManagementTokenObtained(false);
    setSiteCapabilitiesTokenObtained(false);
    setLoggedIn(false);

    var urlCommand: string = APIPrefix() + '/v1/logout';

    try {
      const apiResult = await fetch(urlCommand, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        credentials: 'include',
      });
    } catch (e) {}
  } // logout

  //	------------------------------------------------------------
  //
  //	Call the API to move data to a storage area.
  //
  //	------------------------------------------------------------

  async function moveDataToStorage(args: { settings: MoveDataToStorage }) {
    var urlCommand: string = APIPrefix() + '/v1/data_management/move_data?';

    // token.
    urlCommand =
      urlCommand +
      'to_storage_area_uuid=' +
      args.settings.toStorageAreaUUID +
      '&' +
      'to_site=' +
      args.settings.toSite +
      '&' +
      'to_storage_identifier=' +
      args.settings.toStorageIdentifier +
      '&' +
      'lifetime=' +
      args.settings.lifetime.toString();

    try {
      const apiResult = await fetch(urlCommand, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify(args.settings.filesToMove),
      });
      if (apiResult.status === 201) {
        const returnedJson = await apiResult.json();

        // get job ID.
        var jobID: string = '';
        if (returnedJson.job_id !== undefined) jobID = returnedJson.job_id;

        // update the state with the job ID.
        console.log('Submitted job id:');
        console.log(jobID);

        // refresh the data-management jobs panel.
        const currentTask: CurrentTask = { taskType: TaskType.GET_DATA_MANAGEMENT_JOBS };
        taskExecutor({ currentTask: currentTask });
      }

      // if the return code is 401 then session has expired.
      if (apiResult.status === 401) {
        console.log('moveDataToStorage 401.');
      }

      // if the return code is 403 then we don't have a token. either we're
      // not logged in, or we're not authorised to the data-management API.
    } catch (e) {
      console.log(e);
    }
  } // moveDataToStorage

  //	-------------------------------------------------
  //
  //	handles button click events on the main form.
  //
  //	-------------------------------------------------

  const onClickHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();

    const button: HTMLButtonElement = event.currentTarget;

    // check if the clicked button is one of the tools shown across the top of the page.
    for (var i = 0; i < TOOLS.length; i++)
      if (button.name === TOOLS[i].name) setSelectedTool(TOOLS[i].toolType);

    // if the login button is clicked then display or hide the pop-up login box.
    if (button.name === 'btnLogin')
      // connect to SKA IAM and OIDC.
      redirectToLoginURL();

    // if the user account button is clicked then display or hide the dropdown menu.
    if (button.name === 'btnUserAccount' && sUserDropdownMenuDisplayed === false)
      setUserDropdownMenuDisplayed(true);
    else setUserDropdownMenuDisplayed(false);

    // logout button from the user control dropdown menu.
    if (button.name === 'btnLogout') {
      setUserDropdownMenuDisplayed(false);

      // log out of the backend.
      logout();
    }

    // OIDC token button from the user control dropdown menu.
    if (button.name === 'btnOIDCToken') setSelectedTool(ToolType.OIDC_TOKENS);

    // user preferences dialog
    if (button.name === 'btnPreferences') {
      if (sShowPreferencesDialog === true) setShowPreferencesDialog(false);
      else setShowPreferencesDialog(true);
    }

    // language menu.
    if (button.name === 'lstLanguage' && sLanguageDropdownMenuDisplayed === false)
      setLanguageDropdownMenuDisplayed(true);
    else setLanguageDropdownMenuDisplayed(false);

    // view all notifications.
    if (button.name === 'viewNotifications') setSelectedTool(ToolType.VIEW_NOTIFICATIONS);

    // update the current language.
    if (button.name.length > 12)
      if (button.name.slice(0, 12) === 'btnLanguage_') {
        // get the rest of the text.
        var language: string = button.name.slice(12 - button.name.length);

        // update the language index.
        var languageIndex: number = findLanguageIndex({ language: language });
        setLanguageIndex(languageIndex);

        // update the user-preferences database.
        if (sLoggedIn === true) {
          const settings: UserSettings = { language: language };
          const currentTask: CurrentTask = {
            taskType: TaskType.UPDATE_SETTINGS,
            parameters: settings,
          };
          taskExecutor({ currentTask: currentTask });
        }

        // change the language.
        //i18n.changeLanguage( language );
        setLanguage(language);
      }
  }; // onClickHandler

  //	-------------------------------------------------
  //
  //	handles button click events in div elements
  //	on the main form.
  //
  //	-------------------------------------------------

  const onClickDivHandler = (event: React.MouseEvent<HTMLDivElement>) => {
    const div: HTMLDivElement = event.currentTarget;

    // if one of the project buttons has been pressed then update the currently selected project.
    if (div.id.length > 8)
      if (div.id.slice(0, 8) === 'project_') {
        // extract the selected index.
        const indexStr: string = div.id.slice(8 - div.id.length);

        // convert to number.
        var index: number = 0;
        try {
          index = Number(indexStr);
        } catch (e) {}

        // update state.
        setSelectedProject(index);
      }
  }; // onClickDivHandler

  //	-------------------------------------------------
  //
  //	allows components to raise events when required.
  //
  //	-------------------------------------------------

  function raiseEvent(args: { event: Event }) {
    // can we handle (or partially handle) this event here?
    // set the selected tool when certain events are received.
    if (args.event.eventType === EventType.INITIATE_COMPUTE_SEARCH) {
      const params: InitiateComputeSearchParams = args.event.parameters;
      if (params !== undefined) {
        if (params.showPage === true) {
          setSelectedTool(ToolType.SEARCH_COMPUTE);
        }
      }
    }
    if (args.event.eventType === EventType.INITIATE_DM_SEARCH) {
      const params: InitiateDMSearchParams = args.event.parameters;
      if (params !== undefined) {
        if (params.showPage === true) {
          setSelectedTool(ToolType.DATA_MANAGEMENT);
        }
      }
    }
    if (args.event.eventType === EventType.INITIATE_DATA_SEARCH) {
      setSelectedTool(ToolType.SEARCH_CATALOG);
    }

    // handle a launch notebook event.
    if (
      args.event.eventType === EventType.LAUNCH_NOTEBOOK &&
      args.event.parameters !== undefined
    ) {
      launchNotebook({ url: args.event.parameters });
    }

    // handle a delete notifications event.
    if (
      args.event.eventType === EventType.DELETE_NOTIFICATIONS &&
      args.event.parameters !== undefined
    ) {
      deleteNotifications({ notificationIDs: args.event.parameters });
    }

    // handle a mark notifications read event.
    if (
      args.event.eventType === EventType.MARK_NOTIFICATIONS_READ &&
      args.event.parameters !== undefined
    ) {
      markNotificationsRead({ notificationIDs: args.event.parameters });
    }

    // if the incoming event is global, or destined for a child component, then
    // pass it on.
    if (
      (args.event.eventTarget === EventTarget.GLOBAL ||
        args.event.eventTarget === EventTarget.DATA_MANAGEMENT_TABLE) &&
      sEventToDataManagement !== undefined
    )
      sEventToDataManagement({ event: args.event });
    if (
      (args.event.eventTarget === EventTarget.GLOBAL ||
        args.event.eventTarget === EventTarget.SEARCH_CATALOG ||
        args.event.eventTarget === EventTarget.SEARCH_CATALOG_FORM ||
        args.event.eventTarget === EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL) &&
      sEventToSearchCatalogue !== undefined
    )
      sEventToSearchCatalogue({ event: args.event });
    if (
      (args.event.eventTarget === EventTarget.GLOBAL ||
        args.event.eventTarget === EventTarget.SEARCH_COMPUTE_TABLE) &&
      sEventToSearchCompute !== undefined
    )
      sEventToSearchCompute({ event: args.event });
  } // raiseEvent

  //	-------------------------------------------------
  //
  //	Redirects the browser to the IAM Login URL.
  //
  //	-------------------------------------------------

  async function redirectToLoginURL() {
    try {
      const loginURI =
        'https://authn.srcdev.skao.int/api/v1/login/code?redirect_uri=' + sRedirectURI;
      // redirect the browser to the login uri.
      window.location.href = loginURI;
    } catch (e) {
      console.log('Error redirecting browser to IAM login URL.');
    }
  } // redirectToLoginURL

  //	------------------------------------------------------------
  //
  //	Remove items from a data collection in the database.
  //
  //	------------------------------------------------------------

  async function removeFromDataCollection(args: { dataCollection: DataCollection }) {
    console.log('removeFromDataCollection fires:');
    console.log(args.dataCollection);
    if (args.dataCollection.dataCollection !== undefined)
      if (args.dataCollection.dataCollection.id !== undefined) {
        var urlCommand: string =
          APIPrefix() +
          '/v1/remove_from_data_collection' +
          '?data_collection_id=' +
          args.dataCollection.dataCollection.id.toString();

        try {
          const apiResult = await fetch(urlCommand, {
            method: 'PUT',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            credentials: 'include',
            body:
              args.dataCollection.dids !== undefined
                ? JSON.stringify(args.dataCollection.dids)
                : JSON.stringify([]),
          });

          const returnedJson = await apiResult.json();

          // HTTP 200 means the items have been removed from the collection.
          if (apiResult.status === 200) {
            // get the number of items removed.
            var itemsRemoved: number = 0;
            try {
              itemsRemoved = returnedJson.additional_info.items_removed;
            } catch (e) {}

            // raise a deleted-items-from-collection event.
            const newEvent: Event = {
              eventTarget: EventTarget.GLOBAL,
              eventType: EventType.ITEMS_REMOVED_FROM_DATA_COLLECTION,
            };
            raiseEvent({ event: newEvent });
          }

          // HTTP 401 means the session has expired.
          else if (apiResult.status === 401) {
            console.log('removeFromDataCollection 401.');
          }

          // HTTP 403 means the user is not a member of this project.
          /*else if (apiResult.status === 403) {
          }*/

          // HTTP 404 means a data collection with this id does not exist.
          /* else if (apiResult.status === 404) {
          }*/

          // anything else, then display an error message.
          /*else {
            // TODO: Display error message
          }*/
        } catch (e) {}
      }
  } // removeFromDataCollection

  //	------------------------------------------------------------
  //
  //	Use the backend API to add or update a user-access token.
  //
  //	------------------------------------------------------------

  async function saveUserToken(args: { userServiceToken: UserServiceToken }) {
    try {
      var url =
        APIPrefix() +
        '/v1/set_user_token' +
        '?service_id=' +
        encodeURIComponent(args.userServiceToken.serviceID) +
        '&using_token=' +
        (args.userServiceToken.usingToken === true ? 'Y' : 'N');
      if (args.userServiceToken.usingToken === true)
        url =
          url +
          '&username=' +
          encodeURIComponent(args.userServiceToken.username) +
          '&user_token=' +
          encodeURIComponent(args.userServiceToken.userToken);

      // call the /set_user_token endpoint of the backend API.
      const apiResult = await fetch(url, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
      });

      // HTTP 401 means the session has expired.
      if (apiResult.status === 401) {
        console.log('saveUserToken 401');
      }
    } catch (e) {}
  } // saveUserToken

  //	-------------------------------------------------
  //
  //	apply new settings to the database for this user.
  //
  //	-------------------------------------------------

  async function setPreferences(args: { settings: UserSettings }) {
    try {
      var withParams: boolean = false;
      var url = APIPrefix() + '/v1/set_preferences';
      if (args.settings.darkMode !== undefined) {
        if (withParams === false) {
          url = url + '?';
          withParams = true;
        } else url = url + '&';
        url = url + 'dark_mode=' + (args.settings.darkMode === true ? 'T' : 'F');
      }
      if (args.settings.language !== undefined) {
        if (withParams === false) {
          url = url + '?';
          withParams = true;
        } else url = url + '&';
        url = url + 'language=' + args.settings.language;
      }

      // call the /set_preferences end point of the backend API.
      const apiResult = await fetch(url, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
      });

      // HTTP 401 means the session has expired.
      if (apiResult.status === 401) {
        console.log('setPreferences 401.');
      }
    } catch (e) {}
  } // setPreferences

  //	------------------------------------------------------------
  //
  //	carry out a defined task that requires a valid access token.
  //
  //	------------------------------------------------------------

  async function taskExecutor(args: { currentTask: CurrentTask }) {
    console.log('taskExecutor fires - taskType ' + TaskType[args.currentTask.taskType]);

    // finish loading the user settings.
    if (args.currentTask.taskType === TaskType.LOAD_SETTINGS) loadUserSettings();

    // finish updating the user settings.
    if (
      args.currentTask.taskType === TaskType.UPDATE_SETTINGS &&
      args.currentTask.parameters !== undefined
    )
      setPreferences({ settings: args.currentTask.parameters });

    // finish updating user-service token.
    if (
      args.currentTask.taskType === TaskType.UPDATE_USER_SERVICE_TOKEN &&
      args.currentTask.parameters !== undefined
    )
      saveUserToken({ userServiceToken: args.currentTask.parameters });

    // finish moving data files to storage.
    if (
      args.currentTask.taskType === TaskType.MOVE_DATA_TO_STORAGE &&
      args.currentTask.parameters !== undefined
    )
      moveDataToStorage({ settings: args.currentTask.parameters });

    // get data-management jobs.
    if (args.currentTask.taskType === TaskType.GET_DATA_MANAGEMENT_JOBS)
      getDataManagementJobs();

    // delete notifications from the DB.
    if (
      args.currentTask.taskType === TaskType.DELETE_NOTIFICATIONS &&
      args.currentTask.parameters !== undefined
    )
      deleteNotificationsFromDB({ notificationIDs: args.currentTask.parameters });

    // mark notifications as read in the DB.
    if (
      args.currentTask.taskType === TaskType.MARK_NOTIFICATIONS_READ &&
      args.currentTask.parameters !== undefined
    )
      markNotificationsReadInDB({ notificationIDs: args.currentTask.parameters });

    // add a new data collection.
    if (
      args.currentTask.taskType === TaskType.CREATE_NEW_DATA_COLLECTION &&
      args.currentTask.parameters !== undefined
    )
      createNewDataCollection({ dataCollection: args.currentTask.parameters });

    // add a an existing data collection.
    if (
      args.currentTask.taskType === TaskType.ADD_TO_EXISTING_DATA_COLLECTION &&
      args.currentTask.parameters !== undefined
    )
      addToExistingDataCollection({ dataCollection: args.currentTask.parameters });

    // remove item(s) from an existing data collection.
    if (
      args.currentTask.taskType === TaskType.REMOVE_FROM_DATA_COLLECTION &&
      args.currentTask.parameters !== undefined
    )
      removeFromDataCollection({ dataCollection: args.currentTask.parameters });

    // delete a data collection.
    if (
      args.currentTask.taskType === TaskType.DELETE_DATA_COLLECTION &&
      args.currentTask.parameters !== undefined
    )
      deleteDataCollection({ dataCollection: args.currentTask.parameters });
  } // taskExecutor

  //	-------------------------------------------------
  //
  //	Functions that allows the child components to
  //	update the state of the App function.
  //
  //	-------------------------------------------------

  function updateState(args: { setShowPreferencesDialog?: boolean }) {
    // update the display status of the preferences dialog box.
    if (args.setShowPreferencesDialog !== undefined)
      setShowPreferencesDialog(args.setShowPreferencesDialog);
  } // updateState

  // define hook for changing the language when the state variable changes.
  useEffect(() => {
    // change the language to the that stored within state.
    i18n.changeLanguage(sLanguage);
  }, [sLanguage]);

  useEffect(() => {
    // get the re-direct address from the URL.
    const redirect: string = GetURL();
    setRedirectURI(redirect);

    console.log(
      'useEffect [] - code: ' + authorisationCode + ', sLoggedIn: ' + sLoggedIn,
    );
    console.log('redirect: ' + redirect);
    console.log('api prefix: ' + APIPrefix());

    setLoginTrigger({
      code: authorisationCode !== null ? authorisationCode : '',
      redirectURI: redirect !== null ? redirect : '',
    });

    //const login_params = { code: authorisationCode, redirectURI: redirect };
    //userLogin(login_params);

    // remove the query params from the url.
    window.history.replaceState(null, '', redirect);

    console.log('NODE_ENV:');
    console.log(process.env.NODE_ENV);
  }, []);

  //	-------------------------------------------------
  //
  //	hook that identifies a new auth code, and calls
  //	the /login end point. we do this in a hook to
  //	ensure that /login is called once only when
  //	the auth code changes.
  //
  //	-------------------------------------------------

  useEffect(() => {
    console.log('sLoginTrigger hook fires', sLoginTrigger);
    if (sLoginTrigger.code !== null) userLogin(sLoginTrigger);
  }, [sLoginTrigger.code]);

  // define a hook that loads a list of storage areas and local JupyterHUBs when logged in.
  useEffect(() => {
    if (sLoggedIn === true) {
      // load the list of storage areas from the site-capabilities API.
      loadStorageAreas();
      // load the list of local JupyterHUB services.
      loadJupyterHUBs();
    }
  }, [sLoggedIn]);

  //	-------------------------------------------------
  //
  //	Session Toast
  //
  //	-------------------------------------------------
  async function checkUserSession() {
    try {
      const session_valid = await hasSessionExpired();
      // update the state that holds if the current login session is valid.
      setSessionValid(session_valid);
    } catch (e) {}
  }

  useEffect(() => {
    console.log('User session hook>>>>>>>>>>>>>>>>');
    checkUserSession();
    const refreshInterval = setInterval(checkUserSession, 60000);
    return () => clearInterval(refreshInterval);
  }, []);

  useEffect(() => {
    if (sSessionValid === false && sLoggedIn === true) {
      console.log('User session expired>>>>>>>>>>>>>>>>');
      toast(<SessionToast onLogin={redirectToLoginURL} />);
      // Log the user out
      logout();
    }
  }, [sSessionValid]);

  // extract the base64 for an image.
  //	var base64 = imageUrlToBase64( galaxy32 ).then
  //		(
  //		response =>
  //		{
  //			console.log( "base64: " + response );
  //		}
  //		);
  //console.log( 'data:image/png;base64,' + DISH_64 );

  return (
    <Suspense fallback="loading">
      <div className="page-body" data-theme={sTheme}>
        <div className="header">
          <div className="header-icon">
            <div className="ska-logo-container-dark">
              <img src={srcLogo} alt="" width="190px" />
            </div>
            <div className="ska-logo-container-light">
              <img src={srcLightLogo} alt="" width="190px" />
            </div>
          </div>

          {/* the horizontal icon bar contains all the tools that the user might want to use. */}
          <div className="horizontal-icon-bar">
            {TOOLS.map((item, index) => (
              <Tool
                key={index.toString() + t(item.text) + (sLoadingToken ? 'T' : 'F')}
                name={item.name}
                icon={item.icon}
                text={t(item.text)}
                onClick={onClickHandler}
                selected={sSelectedTool === item.toolType}
                loading={
                  sLoadingToken === true &&
                  (item.needSiteCapabilities === true ||
                    item.needDataManagement === true ||
                    item.needLoggedIn === true ||
                    item.needJupyterURL === true)
                }
                disabled={
                  (item.needSiteCapabilities === true &&
                    sSiteCapabilitiesTokenObtained !== true) ||
                  (item.needDataManagement === true &&
                    sDataManagementTokenObtained !== true) ||
                  (item.needLoggedIn === true && sLoggedIn !== true) ||
                  (item.needJupyterURL === true && sJupyterURL === '')
                }
              />
            ))}

            {/*<Tool	name = "btnVMs"
							icon = {monitorIcon}
							text = {t("VMs")}
							onClick = {onClickHandler}
							selected = {sSelectedTool === ToolType.VIRTUAL_MACHINE}
							disabled = {true} />*/}

            <div className="flex-expander"></div>

            <div className="horizontal-icon-bar-flex-column">
              <div style={{ flex: '0 0 40px' }}></div>
              <UserControl
                key={sUsername + (sLoggedIn ? 'T' : 'F') + sLoginCount}
                loggedIn={sLoggedIn}
                username={sUsername}
                buttonHandler={onClickHandler}
                userDropdownMenuDisplayed={sUserDropdownMenuDisplayed}
                setUserDropdownMenuDisplayed={setUserDropdownMenuDisplayed}
                taskExecutor={taskExecutor}
                notifications={sNotifications}
                setNotifications={setNotifications}
                raiseEvent={raiseEvent}
                isSessionValid={sSessionValid}
                setEventHandler={eventToUserControl}
              />

              <div
                style={{
                  position: 'relative',
                  margin: '0px 10px 0px 10px',
                  display: 'flex',
                  flexDirection: 'column',
                  flex: '0 0 40px',
                  top: '40px',
                  width: '230px',
                }}
              >
                <button
                  className="horizontal-icon-bar-language-holder"
                  name="lstLanguage"
                  type="button"
                  onClick={onClickHandler}
                  data-menu-displayed={sLanguageDropdownMenuDisplayed ? 'T' : 'F'}
                >
                  <div className="language-text">
                    {sLanguageIndex >= 0 ? LANGUAGE_OPTIONS[sLanguageIndex].label : ''}
                  </div>
                  <div className="flex-10px"></div>
                  <img
                    src={
                      sLanguageIndex >= 0
                        ? LANGUAGE_OPTIONS[sLanguageIndex].icon
                        : undefined
                    }
                    alt=""
                    width="30"
                    height="16"
                  />
                  <div className="flex-5px"></div>
                </button>
                <LanguageDropdownMenu
                  sLanguageDropdownMenuDisplayed={sLanguageDropdownMenuDisplayed}
                  setLanguageDropdownMenuDisplayed={setLanguageDropdownMenuDisplayed}
                  buttonHandler={onClickHandler}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="white-horizontal-separator"></div>

        {/* the project space is everything below the tool bar */}
        <div
          style={{
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'row',
            flex: '1 1 0px',
            marginBottom: '10px',
          }}
        >
          {/* we have a project bar down the left-hand side so the user can switch between projects */}
          <Scrollbox
            direction="vertical"
            flex="0 0 110px"
            height="100%"
            flexDirection="column"
          >
            {/* add the blue bar that contains the buttons */}
            <div
              className="project-button-container"
              style={{
                flex: '0 0 auto',
                width: '110px',
                display: 'flex',
                flexDirection: 'column',
                borderRadius: '0px 10px 10px 0px',
                alignItems: 'center',
              }}
            >
              {/* add all the project buttons */}
              <div style={{ flex: '0 0 10px' }} />
              <div
                style={
                  sLoadingProjects === true
                    ? {
                        display: 'flex',
                        flexDirection: 'column',
                        fontSize: '11pt',
                        color: 'white',
                        alignItems: 'center',
                        textAlign: 'center',
                      }
                    : {
                        display: 'none',
                      }
                }
              >
                <img src={gearsIcon} alt="" width="60" height="60" />
                <div style={{ flex: '0 0 5px' }} />
                <div
                  style={{
                    flex: '0 0 auto',
                    display:
                      sLoggedIn === true || sLoggedIn === undefined ? 'flex' : 'none',
                  }}
                >
                  {sLoggedIn === true ? 'Retrieving projects...' : 'Starting up...'}
                </div>
                <div style={{ flex: '0 0 5px' }} />
              </div>
              {sProjects.map((project, index) => (
                <ProjectButton
                  key={index}
                  name={'project_' + index.toString()}
                  icon={project.icon}
                  onClick={onClickDivHandler}
                  selected={sSelectedProject === index}
                  projectName={project.abbreviation.toUpperCase()}
                  description={project.description}
                />
              ))}
              <div style={{ flex: '0 0 10px' }} />
            </div>
            <div className="flex-expander"></div>
          </Scrollbox>
          <div className="white-vertical-separator"></div>

          {/* The home tab */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.HOME ? 'T' : 'F'}
          >
            <Home
              dataManagementJobs={sDataManagementJobs}
              storageAreas={sStorageAreas}
              taskExecutor={taskExecutor}
              jobsLoading={sJobsLoading}
              raiseEvent={raiseEvent}
              jupyterHUBs={sJupyterHUBs}
            />
          </div>

          {/* The search catalog tab for data discovery */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.SEARCH_CATALOG ? 'T' : 'F'}
          >
            <SearchCatalog
              updateState={updateState}
              tabVisible={sSelectedTool === ToolType.SEARCH_CATALOG}
              dataStagingAvailable={sDataManagementTokenObtained}
              taskExecutor={taskExecutor}
              project={
                sSelectedProject >= 0 && sProjects.length > 0
                  ? {
                      id: sProjects[sSelectedProject].id,
                      name: sProjects[sSelectedProject].name,
                    }
                  : undefined
              }
              setEventHandler={eventToSearchCatalogue}
              raiseEvent={raiseEvent}
            />
          </div>

          {/* For debugging only - display the data on the currently logged in user */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.OIDC_TOKENS ? 'T' : 'F'}
          >
            {sSelectedTool === ToolType.OIDC_TOKENS && (
              <UserControlOIDCToken key={sLoginCount} />
            )}
          </div>

          {/* The search compute tab */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.SEARCH_COMPUTE ? 'T' : 'F'}
          >
            <SearchCompute
              loginCount={sLoginCount}
              siteCapabilitiesTokenObtained={sSiteCapabilitiesTokenObtained}
              taskExecutor={taskExecutor}
              tabVisible={sSelectedTool === ToolType.SEARCH_COMPUTE}
              raiseEvent={raiseEvent}
              setEventHandler={eventToSearchCompute}
            />
          </div>

          {/* The data management tab */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.DATA_MANAGEMENT ? 'T' : 'F'}
          >
            <DataManagement
              dataManagementTokenObtained={sDataManagementTokenObtained}
              dataManagementJobs={sDataManagementJobs}
              storageAreas={sStorageAreas}
              project={
                sSelectedProject >= 0 && sProjects.length > 0
                  ? sProjects[sSelectedProject]
                  : undefined
              }
              taskExecutor={taskExecutor}
              jobsLoading={sJobsLoading}
              tabVisible={sSelectedTool === ToolType.DATA_MANAGEMENT}
              raiseEvent={raiseEvent}
              jupyterHUBs={sJupyterHUBs}
              setEventHandler={eventToDataManagement}
            />
          </div>

          {/* The notebook tab */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.NOTEBOOK ? 'T' : 'F'}
          >
            <iframe key={sJupyterRender} className="notebook" src={sJupyterURL} />
          </div>

          {/* The visualisation tab */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.VISUALISE_DATA ? 'T' : 'F'}
          >
            <Visualisation params={buildVisualisationParams()} raiseEvent={raiseEvent} />
          </div>

          {/* The user-management tab */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.USER_MANAGEMENT ? 'T' : 'F'}
          >
            <UserManagement tabVisible={sSelectedTool === ToolType.USER_MANAGEMENT} />
          </div>

          {/* The notifications tab */}
          <div
            className="section"
            data-visible={sSelectedTool === ToolType.VIEW_NOTIFICATIONS ? 'T' : 'F'}
          >
            <ViewNotifications
              taskExecutor={taskExecutor}
              notifications={sNotifications}
              pageDisplayed={sSelectedTool === ToolType.VIEW_NOTIFICATIONS}
              raiseEvent={raiseEvent}
              setEventHandler={eventToViewNotifications}
            />
          </div>

          {/* If none of the above then we display an empty page */}
          <div
            className="section"
            data-visible={
              TOOLS.findIndex((element) => element.toolType === sSelectedTool) === -1 &&
              sSelectedTool !== ToolType.OIDC_TOKENS &&
              sSelectedTool !== ToolType.VIEW_NOTIFICATIONS
                ? 'T'
                : 'F'
            }
          >
            <div className="empty-window"></div>
          </div>
        </div>
        {/*<div className="white-horizontal-separator"></div>*/}

        {/* dim the whole page background if a modal form is being displayed */}
        {sShowPreferencesDialog === true && (
          <div
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'black',
              opacity: '0.3',
              position: 'absolute',
              top: '0px',
              left: '0px',
              border: 'none',
              display: 'flex',
              flexDirection: 'row',
              zIndex: '0',
            }}
          />
        )}

        {/* display the user preferences dialog (if the state flag is set) */}
        {sShowPreferencesDialog === true && (
          <Preferences
            theme={sTheme}
            setTheme={setTheme}
            taskExecutor={taskExecutor}
            updateState={updateState}
          />
        )}
        {/* Session Toast */}
        <ToastContainer
          position="top-right"
          autoClose={false}
          hideProgressBar
          closeOnClick={false}
          pauseOnHover
          draggable={false}
          theme={sTheme}
        />
      </div>
    </Suspense>
  );
} // App

export default App;

import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import Home from '../../components/home/home';
import { JobType, SiteStorageAreas, JupyterHUBs } from '../../utils/types';
import userEvent from '@testing-library/user-event';
import tickIcon from '../../icons/tick.512.png';
import crossIcon from '../../icons/cross.256.png';
import moveFileIcon from '../../icons/move-file.256.png';
import { itemColour, itemIcon } from '../../utils/functions';

//Mock translation hook
jest.mock('react-i18next', () => ({
  useTranslation: () => ({
    t: (key: string) => key,
  }),
}));

describe('Home component rendering', () => {
  const mockDataManagementJobs: JobType[] = [
    {
      row_id: 1,
      job_id: 'test-job-001',
      status: 'Ready',
      to_site: 'SPSRC_STORM',
      to_storage_identifier: 'test-store-identifier-001',
      to_storage_area_uuid: 'test-storage-001',
      parent_namespace: 'test-namespace-1',
      parent_name: 'test-parent-name-1',
      containers: [],
      datasets: [],
      files: [],
      expires_at: '2025-01-14T23:59:00Z',
      expires_in: '1 day',
      num_items: 0,
    },
    {
      row_id: 2,
      job_id: 'test-job-002',
      status: 'Error',
      to_site: 'STFC_STORM',
      to_storage_identifier: 'test-store-identifier-002',
      to_storage_area_uuid: 'test-storage-002',
      parent_namespace: 'test-namespace-2',
      parent_name: 'test-parent-name-2',
      containers: [],
      datasets: [],
      files: [],
      expires_at: '2025-01-14T23:59:00Z',
      expires_in: '1 day',
      num_items: 0,
    },
  ];

  const mockStorageAreas: SiteStorageAreas[] = [
    {
      site: 'SKAOSRC',
      storage_areas: [
        {
          associated_storage_id: 'test-storage-001',
          storage_id: 'test-storage-id-001',
          storage_type: 'rse',
          relative_path: '/deterministic',
          identifier: 'STFC_STORM',
        },
        {
          associated_storage_id: 'test-storage-002',
          storage_id: 'test-storage-id-002',
          storage_type: 'rse',
          relative_path: '/nondeterministic',
          identifier: 'STFC_STORM_ND',
        },
      ],
    },
  ];

  const mockJupyterHUBs: JupyterHUBs[] = [
    {
      site: 'SKAOSRC',
      associated_services: [
        {
          id: 'test-service-001',
          // "type": "jupyterhub",
          path: '',
          prefix: 'http',
          host: 'localhost',
          port: 8000,
          identifier: 'Tangerine local test JupyterHub (embed)',
          // "enabled": true,
          // "other_attributes": {},
          // "is_mandatory": false,
          // "is_proxied": false
        },
      ],
    },
  ];

  // const mockInitiateDataManagementSearchEvent = jest.fn();
  const mockRaiseEvent = jest.fn();

  const homeComponent = () => {
    render(
      <Home
        dataManagementJobs={mockDataManagementJobs}
        storageAreas={mockStorageAreas}
        taskExecutor={{}}
        jobsLoading={false}
        raiseEvent={mockRaiseEvent}
        jupyterHUBs={mockJupyterHUBs}
      />,
    );
    //initiateDataManagementSearchEvent={mockInitiateDataManagementSearchEvent}
  };

  test('renders Home component successfully', async () => {
    homeComponent();

    const textElement = screen.getByText(/Service Status/i);
    await waitFor(() => {
      expect(textElement).toBeInTheDocument();
    });
  });

  test('renders all services and their indicators', async () => {
    homeComponent();
    const service_names = [
      'IAM',
      'Rucio',
      'APIs',
      'Authentication API',
      'Site-capabilities API',
      'Data-management API',
      'Gateway-backend API',
      'Permissions API',
    ];

    await waitFor(() => {
      service_names.forEach((service) => {
        expect(screen.getByText(service)).toBeInTheDocument();
      });
    });
  });

  test('renders Home component and update each service status from API', async () => {
    homeComponent();

    // Wait for the API response and update the service status
    await waitFor(() => {
      // Check the service status div renders
      expect(screen.getByText(/Service Status/i)).toBeInTheDocument();
    });

    await waitFor(() => {
      // Mock 2 services
      // IAM service should be green i.e RUNNING
      const iamIndicator = screen.getByTestId('service-status-indicator-iam');
      expect(iamIndicator).toHaveAttribute('data-value', 'G');
    });

    await waitFor(() => {
      // Rucio service should be red i.e STOPPED
      const rucioIndicator = screen.getByTestId('service-status-indicator-rucio');
      expect(rucioIndicator).toHaveAttribute('data-value', 'R');
    });
  });
});

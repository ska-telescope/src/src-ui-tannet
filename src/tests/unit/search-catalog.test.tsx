import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import SearchCatalogForm from '../../components/search-catalog/search-catalog-form';
import userEvent from '@testing-library/user-event';

//Mock translation hook
jest.mock('react-i18next', () => ({
  useTranslation: () => ({
    t: (key: string) => key,
  }),
}));

describe('SearchCatalogForm Component', () => {
  const mockAddSearchResultsTab = jest.fn();
  const mockUpdateSearchPosition = jest.fn();
  const mockSetEventHandler = jest.fn();
  const mockProjects = {
    id: 1,
    name: 'Mocked Project',
  };

  const scfComponent = () => {
    render(
      <SearchCatalogForm
        addSearchResultsTab={mockAddSearchResultsTab}
        updateSearchPosition={mockUpdateSearchPosition}
        setEventHandler={mockSetEventHandler}
        tabVisible={true}
        project={mockProjects}
      />,
    );
  };

  test('renders SearchCatalogForm component successfully', () => {
    scfComponent();

    //expect(screen.getByText(/Filter/i)).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Source name')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('RA (degrees)')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('DEC (degrees)')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Radius (degrees)')).toBeInTheDocument();
    expect(screen.getByText('Search')).toBeInTheDocument();
  });

  test('populates data product type dropdown with API data', async () => {
    scfComponent();

    await waitFor(() => {
      expect(screen.getByText('cube')).toBeInTheDocument();
    });

    await waitFor(() => {
      expect(screen.getByText('image')).toBeInTheDocument();
    });

    await waitFor(() => {
      expect(screen.getByText('test_file')).toBeInTheDocument();
    });

    await waitFor(() => {
      expect(screen.getByText('<none>')).toBeInTheDocument();
    });
  });

  test('user entering inputs and clicking buttons', async () => {
    const user = userEvent;

    // Render the component
    scfComponent();

    // First simulate entering source name
    const inputSourceName = screen.getByPlaceholderText('Source name');
    user.type(inputSourceName, 'M51');
    expect(inputSourceName).toHaveValue('M51');

    // Simulate entering the degrees
    const inputRA = screen.getByPlaceholderText('RA (degrees)');
    const inputDEC = screen.getByPlaceholderText('DEC (degrees)');
    const inputRadius = screen.getByPlaceholderText('Radius (degrees)');
    user.type(inputRA, '200');
    user.type(inputDEC, '-85');
    user.type(inputRadius, '5');
    expect(inputRA).toHaveValue('200');
    expect(inputDEC).toHaveValue('-85');
    expect(inputRadius).toHaveValue('5');

    // Simulate user clicking search button
    const searchBtn = screen.getByText('Search');
    user.click(searchBtn);
    expect(mockAddSearchResultsTab).toHaveBeenLastCalledWith({
      datasetValue: '',
      raValue: 200,
      decValue: -85,
      radiusValue: 5,
      obsPublisherDidValue: '',
      dataProductType: undefined,
    });
  });

  test('handles resolving source name', async () => {
    const user = userEvent;
    scfComponent();

    // Simulate user entering source name
    const inputSourceName = screen.getByPlaceholderText('Source name');
    user.type(inputSourceName, 'M51');

    // Simulate the user clicking the resolve button
    const resolveBtn = screen.getByText('Resolve');
    user.click(resolveBtn);

    // wait for resolver text to update
    await waitFor(() => {
      expect(
        screen.getByText('Resolved by SIMBAD: M 51, Object Type: Sy2'),
      ).toBeInTheDocument();
    });

    // Ensure RA, DEC and Radius are updated
    const inputRA = screen.getByPlaceholderText('RA (degrees)');
    const inputDEC = screen.getByPlaceholderText('DEC (degrees)');
    const inputRadius = screen.getByPlaceholderText('Radius (degrees)');
    expect(inputRA).toHaveValue('200');
    expect(inputDEC).toHaveValue('-85');
    expect(inputRadius).toHaveValue('5.0');
  });

  test('minimising and maximising of filter box', () => {
    const user = userEvent;

    scfComponent();

    // Simulate user clicking the minimise button
    const minimiseBtn = screen.getByTitle('Minimise filter');
    user.click(minimiseBtn);
    expect(screen.getByTestId('minimisedFilter')).toHaveAttribute('data-maximised', 'F');

    // Simulate user clicking the maximise button
    const maximiseBtn = screen.getByTitle('Maximise filter');
    user.click(maximiseBtn);
    expect(screen.getByTestId('maximisedFilter')).toHaveAttribute('data-maximised', 'T');
  });
});

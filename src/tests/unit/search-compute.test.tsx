import { render, screen, waitFor } from '@testing-library/react';
import SearchCompute from '../../components/search-compute/search-compute';
import { withTranslation } from 'react-i18next';
import { Component } from 'react';
import SearchComputeSiteList from '../../components/search-compute/search-compute-site-list';
import userEvent from '@testing-library/user-event';
import searchComputeServiceTypesList from '../../components/search-compute/search-compute-service-types-list';
import SearchComputeTable from '../../components/search-compute/search-compute-table';

//Mock translation hook
jest.mock('react-i18next', () => ({
  useTranslation: () => ({
    t: (key: string) => key,
    i18n: {
      changeLanguage: jest.fn(),
    },
  }),
  withTranslation: () => (Component: React.ComponentType<any>) => (props: any) => (
    <Component t={(key: string) => key} {...props} />
  ),
}));

describe('SearchCompute Component', () => {
  const mockTaskExecutor = jest.fn();
  const mockSetEventHandler = jest.fn();
  const mockRaiseEvent = jest.fn();
  const mockInputHandler = jest.fn();

  const searchComputeComponent = () => {
    render(
      <SearchCompute
        loginCount={1}
        siteCapabilitiesTokenObtained={true}
        taskExecutor={mockTaskExecutor}
        tabVisible={true}
        raiseEvent={mockRaiseEvent}
        setEventHandler={mockSetEventHandler}
      />,
    );
  };

  test('renders the Search Compute component', async () => {
    searchComputeComponent();

    await waitFor(() => {
      // Check filter box is expanded when the component mount
      expect(screen.getByTestId('maximiseFilter')).toHaveAttribute('data-maximised', 'T');
    });
  });
});

describe('SearchComputeSiteList component', () => {
  const mockChangeSite = jest.fn();

  const SearchComputeSiteListComponent = () => {
    render(
      <SearchComputeSiteList
        key={1}
        changeSite={mockChangeSite}
        placeholderShown={true}
        value={'all'}
      />,
    );
  };

  test('renders correctly with the placeholder', async () => {
    SearchComputeSiteListComponent();

    await waitFor(() => {
      // Check if the dropdown element renders
      const dropdownElement = screen.getByRole('combobox', { name: '' });
      expect(dropdownElement).toBeInTheDocument();
    });

    await waitFor(() => {
      // Check if 'Select site' placeholder is displayed
      const dropdownElement = screen.getByRole('combobox', { name: '' });
      expect(dropdownElement).toHaveAttribute('data-placeholder-shown', 'T');
    });

    await waitFor(() => {
      expect(screen.getByText('Select site')).toBeInTheDocument();
    });
  });

  test('renders site options correctly', async () => {
    SearchComputeSiteListComponent();

    const mockSiteListApi = {
      sites: ['AUSSRC', 'CANSRC', 'CHSRC', 'CNSRC', 'SKAOSRC', 'SWESRC', 'UKSRC'],
    };

    await waitFor(() => {
      mockSiteListApi.sites.forEach((site) => {
        expect(screen.getByText(site)).toBeInTheDocument();
      });
    });
  });

  test('triggers onChange when a site is selected', async () => {
    const user = userEvent;

    SearchComputeSiteListComponent();

    await waitFor(() => {
      expect(screen.getByText('All')).toBeInTheDocument();
    });

    await waitFor(() => {
      // Simulate user selecting a site
      const dropdownElement = screen.getByRole('combobox', { name: '' });
      user.selectOptions(dropdownElement, 'all');

      // Check if the correct site was selected
      expect(dropdownElement).toHaveValue('all');
    });

    await waitFor(() => {
      // Verify the onChange handler was called
      expect(mockChangeSite).toHaveBeenCalledTimes(1);
    });
  });
});

describe('SearchComputeTable Component', () => {
  const searchComputeTableComponent = () => {
    render(
      <SearchComputeTable
        taskExecutor={jest.fn()}
        setState={jest.fn()}
        raiseEvent={jest.fn()}
        setEventHandler={jest.fn()}
      />,
    );
  };
  /*site={'SWESRC'}
        gpu={true}
        largeScratch={false}
        highMemory={true}
        fastScratch={false}
        description={''}
        hardwareType={'container'}
        middlewareVersion={'v0.1'}
        serviceType={'echo'}
        userLatitude={57.6898}
        userLongitude={11.9742}
        taskExecutor={jest.fn()}
        launchNotebook={jest.fn()}
      />,
    );
  };

  /*test('renders the table correctly', async () => {
    searchComputeTableComponent();

    await waitFor(() => {
      // When the scrollbox is visible means the sites have loaded
      expect(screen.getByText('SWESRC')).toBeInTheDocument();
    });
  });*/
});

import { Log, User, UserManager } from 'oidc-client';

export class AuthService {
  public userManager: UserManager;

  public static stsAuthority = 'https://sdc-dev.astron.nl/esap-api/oidc/authenticate/';
  public static clientId = 'implicit';
  public static clientRoot = window.location.origin + '/'; //"http://localhost:4200/";
  public static clientScope = 'openid profile email api';

  constructor() {
    const settings = {
      authority: AuthService.stsAuthority,
      client_id: AuthService.clientId,
      redirect_uri: `${AuthService.clientRoot}signin-callback.html`,
      silent_redirect_uri: `${AuthService.clientRoot}silent-renew.html`,
      post_logout_redirect_uri: `${AuthService.clientRoot}`,
      response_type: 'id_token token',
      scope: AuthService.clientScope,
    };

    this.userManager = new UserManager(settings);
  } // constructor

  public getUser(): Promise<User | null> {
    return this.userManager.getUser();
  } // getUser

  public login(): Promise<void> {
    this.userManager.signinPopup();
    return Promise.resolve();
  } // login

  public renewToken(): Promise<User> {
    return this.userManager.signinSilent();
  } // renewToken

  public logout(): Promise<void> {
    return this.userManager.signoutRedirect();
  } // logout
} // AuthService

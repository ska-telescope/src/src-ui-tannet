import React, { useState, useEffect, useRef } from 'react';
import Select, {
  StylesConfig,
  ActionMeta,
  InputActionMeta,
  SelectInstance,
  components,
} from 'react-select';
import useLocalStorage from 'use-local-storage';
import '../../App.css';
import './search-catalog.css';
import CSS from 'csstype';
import { useTranslation } from 'react-i18next';

// icons
import arrowsIcon from '../../icons/arrows.256.png';
import animatedArrowsIcon from '../../icons/animated-arrows.256.gif';
import infoIcon from '../../icons/info.512.png';

// types relating to tasks.
import { DataCollection } from '../../utils/tasks';
import { TaskType } from '../../utils/tasks';
import { CurrentTask } from '../../utils/tasks';

// functions
import { APIPrefix } from '../../utils/functions';

// classes
import { ToolButtonType } from '../../tools/tool-button';
import ToolButton from '../../tools/tool-button';
import EditableSelect from '../../tools/editable-select';
import { PopupMessage, MessageType } from '../../tools/popup-message';
import { Event, EventType, EventTarget, AddCollectionEvent } from '../../utils/events';

// Select-box custom styles.
import { customStylesDark } from '../../utils/custom-styles-select-box';
import { customStylesLight } from '../../utils/custom-styles-select-box';

//	--------------------------------------------------------------------------
//
//	T Y P E S
//
//	--------------------------------------------------------------------------

//	------------------------------------------------------------
//
//	when tasks are executed we set their refresh status, to
//	determine whether a refresh will be attempted if a token
//	was found to have expired.
//
//	------------------------------------------------------------

export enum DataCollectionPanelNotificationType {
  COLLECTION_ADDED,
  COLLECTION_ALREADY_EXISTS,
  USER_NOT_PROJECT_MEMBER_NEW_COLLECTION,
  ITEMS_ADDED_TO_COLLECTION,
  COLLECTION_DOES_NOT_EXIST,
  USER_NOT_PROJECT_MEMBER_EXISTING_COLLECTION,
} // DataCollectionPanelNotificationType

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	H T M L   C U S T O M   C O M P O N E N T S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	C L A S S   D E F I N I T I O N
//
//	--------------------------------------------------------------------------

export default function DataCollectionPanel(props: {
  tabVisible: boolean;
  selectedRows: {
    row: number;
    namespace: string;
    filename: string;
  }[];
  project:
    | {
        id: number;
        name: string;
      }
    | undefined;
  taskExecutor: any;
  setEventHandler: any;
}) {
  //	--------------------------------------------------------------------------
  //
  //	C O N S T A N T S
  //
  //	--------------------------------------------------------------------------

  //	--------------------------------------------------------------------------
  //
  //	D E C L A R A T I O N S
  //
  //	--------------------------------------------------------------------------

  // get default user preferences.
  const DEFAULT_DARK = window.matchMedia('(prefers-color-scheme: dark)').matches;

  // translation function
  const { i18n, t } = useTranslation();

  // light/dark mode theme.
  const [sTheme, setTheme] = useLocalStorage(
    'gateway_theme',
    DEFAULT_DARK ? 'dark' : 'light',
  );

  const [sNewCollectionName, setNewCollectionName] = useState<string>('');
  const [sExistingCollection, setExistingCollection] = useState<{
    value: number;
    label: string;
  }>({ value: -1, label: '' });
  const [sExistingCollectionText, setExistingCollectionText] = useState<string>('');

  const [sCollections, setCollections] = useState<
    {
      collectionID: number;
      project: string;
      name: string;
      createdAt: string;
      updatedAt: string;
    }[]
  >([]);
  const [sCollectionListOptions, setCollectionListOptions] = useState<
    {
      value: number;
      label: string;
    }[]
  >([]);

  // filter box maximised, or not?
  const [sFilterMaximised, setFilterMaximised] = useState<boolean>(true);

  //	--------------------------------------------------------------------------
  //
  //	E V E N T   D E C L A R A T I O N S
  //
  //	--------------------------------------------------------------------------

  //	------------------------------------------------------------
  //
  //	raise a create-collection popup message.
  //
  //	------------------------------------------------------------

  const [sRaiseCreateCollectionMessage, setRaiseCreateCollectionMessage] =
    useState<any>(undefined);

  const setCreateCollectionMessageObject = (newFunction: any) => {
    setRaiseCreateCollectionMessage(() => newFunction);
  }; // setCreateCollectionMessageObject

  //	------------------------------------------------------------
  //
  //	raise a new-collection-name popup message.
  //
  //	------------------------------------------------------------

  const [sRaiseNewCollectionNameMessage, setRaiseNewCollectionNameMessage] =
    useState<any>(undefined);
  const [sNewCollectionNameError, setNewCollectionNameError] = useState<boolean>(false);

  const setNewCollectionNameMessageObject = (newFunction: any) => {
    setRaiseNewCollectionNameMessage(() => newFunction);
  }; // setNewCollectionNameMessageObject
  //	--------------------------------------------------------------------------

  //	------------------------------------------------------------
  //
  //	raise an add-to-collection popup message.
  //
  //	------------------------------------------------------------

  const [sRaiseAddToCollectionMessage, setRaiseAddToCollectionMessage] =
    useState<any>(undefined);

  const setAddToCollectionMessageObject = (newFunction: any) => {
    setRaiseAddToCollectionMessage(() => newFunction);
  }; // setAddToCollectionMessageObject

  //	------------------------------------------------------------
  //
  //	raise an existing-collection-name popup message.
  //
  //	------------------------------------------------------------

  const [sRaiseExistingCollectionNameMessage, setRaiseExistingCollectionNameMessage] =
    useState<any>(undefined);
  const [sExistingCollectionNameError, setExistingCollectionNameError] =
    useState<boolean>(false);

  const setExistingCollectionNameMessageObject = (newFunction: any) => {
    setRaiseExistingCollectionNameMessage(() => newFunction);
  }; // setExistingCollectionNameMessageObject

  //	--------------------------------------------------------------------------
  //
  //	F U N C T I O N S
  //
  //	--------------------------------------------------------------------------

  //	------------------------------------------------------------
  //
  //	handle event coming from the parent component.
  //
  //	------------------------------------------------------------

  function eventHandler(args: { event: Event }) {
    console.log(
      'data-collection-panel event handler fires',
      EventType[args.event.eventType],
    );

    // collection added successfully.
    if (args.event.eventType === EventType.DATA_COLLECTION_ADDED) {
      // get event parameters.
      const params: AddCollectionEvent = args.event.parameters;

      // open an INFO message.
      if (
        sRaiseCreateCollectionMessage !== undefined &&
        params.errorTarget === EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
      )
        sRaiseCreateCollectionMessage({
          messageText:
            'Data collection ' +
            (params.collectionName !== undefined
              ? "'" + params.collectionName + "' "
              : '') +
            'added with ' +
            (params.itemsAdded !== undefined ? params.itemsAdded.toString() : '0') +
            ' item(s)',
          messageType: MessageType.INFO,
          timeout: 5000,
        });

      // either load the collections or clear the list.
      if (props.project !== undefined) {
        if (props.project.name !== '')
          loadCollections({
            project: props.project.name,
            lookUpCollection: params.collectionName,
          });
        else setCollections([]);
      } else setCollections([]);

      // clear the input box.
      setNewCollectionName('');
    }

    // collection already exists. open an ERROR message.
    if (args.event.eventType === EventType.DATA_COLLECTION_ALREADY_EXISTS) {
      // get event parameters.
      const params: AddCollectionEvent = args.event.parameters;

      if (
        sRaiseNewCollectionNameMessage !== undefined &&
        params.errorTarget === EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
      )
        sRaiseNewCollectionNameMessage({
          messageText:
            'Data collection ' +
            (params.collectionName !== undefined
              ? "'" + params.collectionName + "' "
              : '') +
            'already exists',
          messageType: MessageType.ERROR,
          timeout: 5000,
        });
    }

    // collection does not exist. open an ERROR message.
    if (args.event.eventType === EventType.DATA_COLLECTION_DOES_NOT_EXIST) {
      // get event parameters.
      const params: AddCollectionEvent = args.event.parameters;

      if (
        sRaiseExistingCollectionNameMessage !== undefined &&
        params.errorTarget === EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
      )
        sRaiseExistingCollectionNameMessage({
          messageText:
            'Data collection ' +
            (params.collectionName !== undefined
              ? "'" + params.collectionName + "' "
              : '') +
            'does not exist',
          messageType: MessageType.ERROR,
          timeout: 5000,
        });
    }

    // user not member of project when adding collection. open an ERROR message.
    if (args.event.eventType === EventType.USER_NOT_PROJECT_MEMBER_NEW_COLLECTION) {
      // get event parameters.
      const params: AddCollectionEvent = args.event.parameters;

      if (
        sRaiseExistingCollectionNameMessage !== undefined &&
        params.errorTarget === EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
      )
        sRaiseExistingCollectionNameMessage({
          messageText:
            'You are not a member of project ' +
            (params.projectName !== undefined ? "'" + params.projectName + "'" : ''),
          messageType: MessageType.ERROR,
          timeout: 5000,
        });
    }

    // user not member of project when adding items to a collection. open an ERROR message.
    if (args.event.eventType === EventType.USER_NOT_PROJECT_MEMBER_EXISTING_COLLECTION) {
      // get event parameters.
      const params: AddCollectionEvent = args.event.parameters;

      if (
        sRaiseAddToCollectionMessage !== undefined &&
        params.errorTarget === EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
      )
        sRaiseAddToCollectionMessage({
          messageText:
            'You are not a member of project ' +
            (params.projectName !== undefined ? "'" + params.projectName + "'" : ''),
          messageType: MessageType.ERROR,
          timeout: 5000,
        });
    }

    // items added to collection successfully. open an INFO message.
    if (args.event.eventType === EventType.ITEMS_ADDED_TO_DATA_COLLECTION) {
      // get event parameters.
      const params: AddCollectionEvent = args.event.parameters;

      if (
        sRaiseAddToCollectionMessage !== undefined &&
        params.errorTarget === EventTarget.SEARCH_CATALOG_DATA_COLLECTION_PANEL
      )
        sRaiseAddToCollectionMessage({
          messageText:
            (params.itemsAdded !== undefined ? params.itemsAdded.toString() : '0') +
            ' item(s) added to data collection ' +
            (params.collectionName !== undefined
              ? "'" + params.collectionName + "' "
              : ''),
          messageType: MessageType.INFO,
          timeout: 5000,
        });
    }

    // a collection has been removed. refresh the collection list.
    if (args.event.eventType === EventType.DATA_COLLECTION_DELETED) {
      if (props.project !== undefined) {
        if (props.project.name !== '') {
          loadCollections({ project: props.project.name });
        } else setCollections([]);
      } else setCollections([]);
    }
  } // eventHandler

  //	------------------------------------------------------------
  //
  //	build a list of items for the existing collection Selectbox.
  //
  //	------------------------------------------------------------

  function getItems(args: {
    collections: {
      collectionID: number;
      project: string;
      name: string;
      createdAt: string;
      updatedAt: string;
    }[];
  }) {
    var values: {
      value: number;
      label: string;
    }[] = [];

    for (var i: number = 0; i < args.collections.length; i++) {
      const newItem: {
        value: number;
        label: string;
      } = {
        value: args.collections[i].collectionID,
        label: args.collections[i].name,
      };
      values.push(newItem);
    }

    // return something.
    return values;
  } // getItems

  //	------------------------------------------------------------
  //
  //	An asynchronous function that loads the collection data
  //	from the back end.
  //
  //	------------------------------------------------------------

  async function loadCollections(args: { project?: string; lookUpCollection?: string }) {
    console.log('loadCollections fires:', args.project, args.lookUpCollection);
    if (args.project !== undefined && args.project !== '') {
      var urlCommand: string =
        APIPrefix() + '/v1/get_data_collections?project=' + args.project;

      try {
        const apiResult = await fetch(urlCommand, {
          headers: { 'Content-Type': 'application/json' },
          credentials: 'include',
        });

        // Return code 200 means the API has run.
        if (apiResult.status === 200) {
          const returnedJson = await apiResult.json();

          // get collection list.
          var collectionList: {
            collectionID: number;
            project: string;
            name: string;
            createdAt: string;
            updatedAt: string;
          }[] = [];
          if (returnedJson.data_collections !== undefined)
            for (var i = 0; i < returnedJson.data_collections.length; i++) {
              var newCollection: {
                collectionID: number;
                project: string;
                name: string;
                createdAt: string;
                updatedAt: string;
              } = {
                collectionID: returnedJson.data_collections[i].collection_id,
                project: returnedJson.data_collections[i].project,
                name: returnedJson.data_collections[i].name,
                createdAt: returnedJson.data_collections[i].created_at,
                updatedAt: returnedJson.data_collections[i].updated_at,
              };
              collectionList.push(newCollection);
            }

          // update the collection list options.
          setCollectionListOptions(
            getItems({
              collections: collectionList,
            }),
          );

          // look for the collection passed as a parameter.
          if (args.lookUpCollection !== undefined && args.lookUpCollection !== '') {
            const lookUpCollection: string = args.lookUpCollection;

            const index: number = collectionList.findIndex(
              (item) => item.name.toUpperCase() === lookUpCollection.toUpperCase(),
            );
            if (index > -1) {
              setExistingCollectionText(args.lookUpCollection);
              setExistingCollection({
                value: collectionList[index].collectionID,
                label: collectionList[index].name,
              });
            }
          }
        }
      } catch (e) {}
    }
  } // loadCollections

  const clearTimerRef = useRef<any>();

  //	------------------------------------------------------------
  //
  //	Handler for input box onChange event.
  //
  //	------------------------------------------------------------

  const onChangeInputHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputBox: HTMLInputElement = event.target;

    // update the state for any input boxes that have been changed.
    if (inputBox.id === 'newCollectionNameDiv') setNewCollectionName(inputBox.value);
  }; // onChangeInputHandler

  //	------------------------------------------------------------
  //
  //	Handler for Select box onChange event.
  //
  //	------------------------------------------------------------

  const onChangeSelectHandler = (option: any) => {
    // raise an onChange event.
    if (option !== null) {
      setExistingCollection(option);
      setExistingCollectionText(option.label);
    } else {
      setExistingCollection({ value: -1, label: '' });
      setExistingCollectionText('');
    }
  }; // onChangeSelectHandler

  //	------------------------------------------------------------
  //
  //	Handler for button onClick events.
  //
  //	------------------------------------------------------------

  const onClickButtonHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    const button: HTMLButtonElement = event.currentTarget;

    // minimise/maximise the filter box if the user clicks minimise/maximise.
    if (button.name === 'minimiseFilter') setFilterMaximised(false);
    if (button.name === 'maximiseFilter') setFilterMaximised(true);

    // create a new data collection.
    if (button.name === 'createCollection') {
      // build list of dids from the selected items.
      var dids: {
        namespace: string;
        filename: string;
      }[] = [];
      for (var i: number = 0; i < props.selectedRows.length; i++)
        if (
          props.selectedRows[i].namespace !== '' &&
          props.selectedRows[i].filename !== ''
        )
          dids.push({
            namespace: props.selectedRows[i].namespace,
            filename: props.selectedRows[i].filename,
          });

      // create a new data collection.
      const dataCollection: DataCollection = {
        project: props.project,
        dataCollection: {
          id: -1,
          name: sNewCollectionName,
        },
        dids: dids,
        messageSearchCatalog: true,
      };
      const currentTask: CurrentTask = {
        taskType: TaskType.CREATE_NEW_DATA_COLLECTION,
        parameters: dataCollection,
      };

      // run the data-management search.
      props.taskExecutor({ currentTask: currentTask });
    }

    // add to an existing data collection.
    if (button.name === 'addCollection') {
      // build list of dids from the selected items.
      var dids: {
        namespace: string;
        filename: string;
      }[] = [];
      for (var i: number = 0; i < props.selectedRows.length; i++)
        if (
          props.selectedRows[i].namespace !== '' &&
          props.selectedRows[i].filename !== ''
        )
          dids.push({
            namespace: props.selectedRows[i].namespace,
            filename: props.selectedRows[i].filename,
          });

      // add to an existing data collection.
      const dataCollection: DataCollection = {
        dataCollection: {
          id: sExistingCollection !== undefined ? sExistingCollection.value : -1,
          name: sExistingCollection !== undefined ? sExistingCollection.label : '',
        },
        project: props.project,
        dids: dids,
        messageSearchCatalog: true,
      };
      const currentTask: CurrentTask = {
        taskType: TaskType.ADD_TO_EXISTING_DATA_COLLECTION,
        parameters: dataCollection,
      };

      // run the data-management search.
      props.taskExecutor({ currentTask: currentTask });
    }
  }; // onClickButtonHandler

  //	------------------------------------------------------------
  //
  //	Handler for Select box onInputChange event.
  //
  //	------------------------------------------------------------

  const onInputChangeSelectHandler = (inputValue: string, meta: InputActionMeta) => {
    // onBlur => setInputValue to last selected value
    //if (meta.action === "input-blur")
    //{
    //	setExistingCollectionText( sExistingCollection ? sExistingCollection.label : '' );
    //	console.log( sExistingCollection );
    //}

    const items: { value: number; label: string }[] = getItems({
      collections: sCollections,
    });

    if (meta.action === 'input-change') {
      // find this item in the list of options.
      const index: number = items.findIndex(
        (item) => item.label.toUpperCase() === inputValue.toUpperCase(),
      );

      // update the text in the Select box.
      setExistingCollectionText(inputValue);

      // update the selected collection if the text exactly matches.
      if (index > -1) setExistingCollection(items[index]);
      else setExistingCollection({ value: -1, label: '' });
    }
  }; // onInputChangeSelectHandler

  // handle changes in size to the tab bar.
  /*	const moveNotificationPopup =	React.useRef<MutationObserver>
					(
						new MutationObserver
						(
							function( mutations )
							{
								for (var mutation of mutations)
									console.log( Date.now().toString() );
							}
						)
					);

	// create a reference to the HTML Div element that contains the tab bar, so that we can monitor for changes in size.
	const createCollectionButton =	React.useCallback
						(
							(container: HTMLDivElement) =>
							{
								
								// check that this reference has actually been assigned to an element, and switch on observing.
		    						if (container !== null)
		    						{
									var parentElement: any = container.parentElement;
									moveNotificationPopup.current.observe( parentElement,	{
																	attributes: true,
																	childList: true,
																	subtree: true
																	} );
								}
		    							
								// When element is unmounted, ref callback is called with a null argument
								// => best time to cleanup the observer
		    						else
									if (moveNotificationPopup.current)
			    							moveNotificationPopup.current.disconnect();
		    							
							}, [moveNotificationPopup.current]
						);
						
// Options for the observer (which mutations to observe)
const config = { attributes: true, childList: false, subtree: false };

// Callback function to execute when mutations are observed
const callback = function(mutationsList: any, observer: any) {
    // Use traditional 'for loops' for IE 11
    for (const mutation of mutationsList) {
        if (mutation.type === 'childList') {
            console.log('Mutations: A child node has been added or removed.');
        }
        else if (mutation.type === 'attributes') {
            console.log('Mutations: A ' + mutation.attributeName + ' attribute was modified.');
        }
    }
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);

// Start observing the target node for configured mutations.
var targetNode: HTMLDivElement | null = document.querySelector('#createCollectionDiv');
if (targetNode !== null)
{
	var parentElement: any = targetNode.parentElement;
	observer.observe(targetNode, config);
}*/

  //	--------------------------------------------------------------------------
  //
  //	C O M P O N E N T S
  //
  //	--------------------------------------------------------------------------

  //	--------------------------------------------------------------------------
  //
  //	H O O K S
  //
  //	--------------------------------------------------------------------------

  //	------------------------------------------------------------
  //
  //	check for a change in the project, and update the list
  //	of existing data collections.
  //
  //	------------------------------------------------------------

  useEffect(() => {
    // either load the collections or clear the list.
    if (props.project !== undefined) {
      if (props.project.name !== '') loadCollections({ project: props.project.name });
      else setCollections([]);
    } else setCollections([]);
  }, [props.project]);

  //	------------------------------------------------------------
  //
  //	check for a change in the list of collections, and make sure
  //	the existing-collections combobox is not empty.
  //
  //	------------------------------------------------------------

  useEffect(() => {
    if (sCollections.length > 0 && sExistingCollection.value === -1)
      setExistingCollection({
        value: sCollections[0].collectionID,
        label: sCollections[0].name,
      });
  }, [sCollections]);

  //	------------------------------------------------------------
  //
  //	set the event handler when the page starts up.
  //
  //	------------------------------------------------------------

  useEffect(() => {
    if (
      sRaiseCreateCollectionMessage !== undefined &&
      sRaiseNewCollectionNameMessage !== undefined &&
      sRaiseAddToCollectionMessage !== undefined &&
      sRaiseExistingCollectionNameMessage !== undefined
    )
      props.setEventHandler(eventHandler);
  }, [
    sRaiseCreateCollectionMessage,
    sRaiseNewCollectionNameMessage,
    sRaiseAddToCollectionMessage,
    sRaiseExistingCollectionNameMessage,
  ]);

  //	--------------------------------------------------------------------------
  //
  //	C O M P O N E N T   C O D E
  //
  //	--------------------------------------------------------------------------

  return (
    <div
      style={{
        flex: '0 0 auto',
        height: 'auto',
        display: 'flex',
        flexDirection: 'column',
        margin: '0px',
      }}
    >
      {/* the minimised panel */}
      <button
        className="minimised-filter"
        name="maximiseFilter"
        type="button"
        title="Maximise filter"
        onClick={onClickButtonHandler}
        data-testid="maximisedFilter"
        data-maximised={sFilterMaximised === true ? 'T' : 'F'}
      >
        <div className="flex-15px"></div>
        <div className="maximise">&raquo;</div>
        <div className="flex-row">
          <div className="rotated-text-box">{t('Data collections')}</div>
        </div>
      </button>

      {/* the maximised panel */}
      <div
        className="search-form-visible"
        data-testid="minimisedFilter"
        data-maximised={sFilterMaximised === true ? 'T' : 'F'}
        style={{ overflow: 'visible' }}
      >
        <div style={{ flex: '0 0 15px' }} />

        {/* panel title */}
        <div
          style={{
            flex: '1 1',
            display: 'flex',
            flexDirection: 'row',
          }}
        >
          <div className="search-catalog-form-title" style={{ flex: '1 1' }}>
            {t('Data collections')}
          </div>
          <div style={{ flex: '0 0 10px' }} />
          <button
            className="minimise"
            name="minimiseFilter"
            type="button"
            title="Minimise filter"
            onClick={onClickButtonHandler}
          >
            &laquo;
          </button>
          <div style={{ flex: '0 0 15px' }}></div>
        </div>
        <div style={{ flex: '0 0 15px' }} />

        <div
          className="search-catalog-text"
          style={{ flex: '0 0 auto', margin: '0px 10px 0px 10px' }}
        >
          {props.selectedRows.length} items(s) selected
        </div>
        <div style={{ flex: '0 0 25px' }} />

        {/*<div	className = 'search-catalog-text'
		    	    		style = {{ flex: '0 0 auto', margin: '0px 10px 0px 10px' }} >
		    	    		New data collection:
		    	    	</div>
		    	    	<div	style = {{ flex: '0 0 10px' }} />*/}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            flex: '0 0 auto',
            margin: '0px 10px 0px 10px',
          }}
        >
          <input
            id="newCollectionNameDiv"
            className="input-field"
            type="text"
            maxLength={32}
            onChange={onChangeInputHandler}
            disabled={props.selectedRows.length === 0}
            data-disabled={props.selectedRows.length === 0 ? 'T' : 'F'}
            data-error={sNewCollectionNameError === true ? 'T' : 'F'}
            value={sNewCollectionName}
            placeholder={t('New data collection')}
          />
        </div>
        <div style={{ flex: '0 0 10px' }} />

        <div /*ref = {createCollectionButton}*/
          id="createCollectionDiv"
          style={{
            flex: '0 0 auto',
            display: 'flex',
            flexDirection: 'row',
            margin: '0px 10px 0px 10px',
            position: 'relative',
          }}
        >
          <ToolButton
            key={t('Create') + (props.tabVisible ? 't' : 'f')}
            name="createCollection"
            onClick={onClickButtonHandler}
            tooltip="Create a new data collection"
            text={t('Create')}
            type={ToolButtonType.SECONDARY}
            disabled={props.selectedRows.length === 0 || sNewCollectionName === ''}
          />
        </div>
        <div style={{ flex: '0 0 10px' }} />
        <div style={{ flex: '0 0 25px' }} />

        {/*<div	className = 'search-catalog-text'
		    	    		style = {{ flex: '0 0 auto', margin: '0px 10px 0px 10px' }} >
		    	    		Existing data collection:
		    	    	</div>
		    	    	<div	style = {{ flex: '0 0 10px' }} />*/}
        <div
          id="existingCollectionNameDiv"
          style={{
            alignItems: 'center',
            flex: '0 0 auto',
            margin: '0px 10px 0px 10px',
          }}
        >
          <EditableSelect
            styles={sTheme === 'dark' ? customStylesDark : customStylesLight}
            isDisabled={props.selectedRows.length === 0}
            options={sCollectionListOptions}
            placeholder={t('Existing data collection')}
            value={sExistingCollection}
            onChange={onChangeSelectHandler}
            inputValue={sExistingCollectionText}
            onInputChange={onInputChangeSelectHandler}
          />
        </div>
        <div style={{ flex: '0 0 10px' }} />

        <div
          id="addToCollectionDiv"
          style={{
            flex: '0 0 auto',
            display: 'flex',
            flexDirection: 'row',
            margin: '0px 10px 0px 10px',
          }}
        >
          <ToolButton
            key={t('Add to') + (props.tabVisible ? 't' : 'f')}
            name="addCollection"
            onClick={onClickButtonHandler}
            tooltip="Add to an existing data collection"
            text={t('Add to')}
            type={ToolButtonType.SECONDARY}
            disabled={props.selectedRows.length === 0 || sExistingCollection.value === -1}
          />
        </div>
        <div style={{ flex: '0 0 25px' }} />
      </div>

      <PopupMessage
        nearComponent="#createCollectionDiv"
        posOffset={{
          left: 0,
          top: -66,
        }}
        setSetMessage={setCreateCollectionMessageObject}
      />

      <PopupMessage
        nearComponent="#newCollectionNameDiv"
        posOffset={{
          left: 0,
          top: -66,
        }}
        setSetMessage={setNewCollectionNameMessageObject}
        setErrorState={setNewCollectionNameError}
      />

      <PopupMessage
        nearComponent="#addToCollectionDiv"
        posOffset={{
          left: 0,
          top: -66,
        }}
        setSetMessage={setAddToCollectionMessageObject}
      />

      <PopupMessage
        nearComponent="#existingCollectionNameDiv"
        posOffset={{
          left: 0,
          top: -66,
        }}
        setSetMessage={setExistingCollectionNameMessageObject}
        setErrorState={setNewCollectionNameError}
      />
    </div>
  );
} // DataCollectionPanel

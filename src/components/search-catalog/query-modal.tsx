import { useState, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
import { DataQuery } from '../../utils/types';
import ToolButton, { ToolButtonType } from '../../tools/tool-button';
import { useTranslation } from 'react-i18next';
import '../../App.css';
import './search-catalog.css';
import './save-query-modal.css';
import 'bootstrap/dist/css/bootstrap.min.css';

interface QueryModalProps {
  show: boolean;
  handleClose: () => void;
  query: DataQuery;
  reRunQuery: (args: { ra: string; dec: string; radius: string }) => void;
}

const QueryModal: React.FC<QueryModalProps> = ({
  show,
  handleClose,
  query,
  reRunQuery,
}) => {
  // translation function
  const { i18n, t } = useTranslation();
  const [theme, setTheme] = useState('light');

  useEffect(() => {
    const storedTheme = localStorage.getItem('gateway_theme') || 'light';
    setTheme(storedTheme);
  }, [theme]);

  const handleReRunQuery = () => {
    console.log('Re-running query:', query);
    const queryString = query.data_query.split('?')[1] || '';

    const params = new URLSearchParams(queryString);
    console.log(
      'RA ' +
        params.get('ra') +
        ' DEC ' +
        params.get('dec') +
        ' Radius ' +
        params.get('radius'),
    );
    const ra = params.get('ra')?.trim() || '0';
    const dec = params.get('dec')?.trim() || '0';
    const radius = params.get('radius')?.trim() || '0';

    reRunQuery({ ra, dec, radius });
    handleClose(); // Close modal after triggering the action
  };

  const modalStyle = {
    backgroundColor: theme === 'dark' ? '#252858' : '#fff',
    color: theme === 'dark' ? '#fff' : '#000',
    borderRadius: '0px',
    border: theme === 'dark' ? '1px solid #252858' : '1px solid #ddd',
  };

  return (
    <Modal show={show} onHide={handleClose} centered>
      <div className="modal-content" style={modalStyle}>
        <Modal.Header closeButton>
          <Modal.Title>Run Saved Query</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Would you like to run this saved query?</p>
          <strong>{query.name}</strong>
        </Modal.Body>
        <Modal.Footer>
          <ToolButton
            key={t('Close')}
            name="close"
            onClick={handleClose}
            tooltip="Close fetch query modal"
            text={t('Close')}
            type={ToolButtonType.SECONDARY}
          />
          <ToolButton
            key={t('ReRun')}
            name="Rerun"
            onClick={handleReRunQuery}
            tooltip="Run saved query modal"
            text={t('Run Query')}
            type={ToolButtonType.PRIMARY}
          />
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export default QueryModal;

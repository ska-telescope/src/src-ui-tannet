import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { fetchDataQueries } from '../../middleware/api/data_query_apis';
import { DataQuery } from '../../utils/types';
import '../../App.css';
import './search-catalog.css';

interface FetchDataQueriesDropdownProps {
  //onChange: (query: DataQuery) => void;
  placeholderShown: boolean;
  value: string | undefined;
  queries: DataQuery[];
  setQueries: (query: DataQuery[]) => void;
  onQuerySelect: any;
}

const FetchDataQueriesDropdown: React.FC<FetchDataQueriesDropdownProps> = ({
  placeholderShown,
  value,
  queries,
  onQuerySelect,
}) => {
  const { t } = useTranslation();

  return (
    <select
      name="fetchDataQueries"
      className="data-product-types-listbox"
      value={value}
      multiple={false}
      onChange={(event) => {
        const selectedQuery = queries.find(
          (q) => q.data_query_id.toString() === event.target.value,
        );
        if (selectedQuery) {
          //onChange(selectedQuery);
          onQuerySelect(selectedQuery);
        }
      }}
      data-placeholder-shown={placeholderShown ? 'T' : 'F'}
    >
      <option hidden value="">
        {t('Select saved query')}
      </option>
      {queries.map((query) => (
        <option key={query.data_query_id} value={query.data_query_id.toString()}>
          {query.name}
        </option>
      ))}
    </select>
  );
};

export default FetchDataQueriesDropdown;

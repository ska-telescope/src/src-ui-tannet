import { useState, useEffect } from 'react';
import { Modal, Form } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  fetchDataQueries,
  ResolveSourceName,
  saveDataQuery,
} from '../../middleware/api/data_query_apis';
import '../../App.css';
import './search-catalog.css';
import './save-query-modal.css';
import { useTranslation } from 'react-i18next';
import ToolButton, { ToolButtonType } from '../../tools/tool-button';
import { DataQuery } from '../../utils/types';

interface SaveQueryModalProps {
  show: boolean;
  handleClose: () => void;
  sourceName: string;
  ra: number;
  dec: number;
  radius: number;
  project: { id: number; name: string } | undefined;
  setQueries: (queries: DataQuery[]) => void;
}

const SaveQueryModal: React.FC<SaveQueryModalProps> = ({
  show,
  handleClose,
  sourceName,
  ra,
  dec,
  radius,
  project,
  setQueries,
}) => {
  // translation function
  const { i18n, t } = useTranslation();

  const [name, setName] = useState('');
  const [queryParam, setQueryParam] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [theme, setTheme] = useState(localStorage.getItem('gateway_theme') || 'light');
  // this state vairable will be used to resolve source name
  const [resolvedSourceName, setResolvedSourceName] = useState(false);

  useEffect(() => {
    const storedTheme = localStorage.getItem('gateway_theme') || 'light';
    setTheme(storedTheme);
  }, [show]);

  useEffect(() => {
    if (!show) {
      setError('');
      setName('');
    }
  }, [show]);

  useEffect(() => {
    document.body.setAttribute('data-theme', theme);
  }, [theme]);

  useEffect(() => {
    const params = new URLSearchParams();
    if (sourceName) params.append('source_name', sourceName);
    if (ra) params.append('ra', ra.toString());
    if (dec) params.append('dec', dec.toString());
    if (radius) params.append('radius', radius.toString());

    setQueryParam('/v1/data/get_product_types?' + params.toString());
  }, [sourceName, ra, dec, radius, resolvedSourceName]);

  // TODO: During innovation sprint
  //const saveQueryMutation = useSaveQuery();

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
    if (e.target.value.trim() !== '') {
      setError('');
    }
  };

  const handleSave = async () => {
    setError('');
    if (!name.trim()) {
      console.log('Please enter a name for the query.');
      setError('Please enter a name for the query.');
      return;
    }

    setLoading(true);

    try {
      let updatedRa = ra;
      let updatedDec = dec;

      // if the user add source name only, resolve it
      if (sourceName && (!ra || (!dec && !radius))) {
        const resolvedData = await ResolveSourceName(sourceName);

        if (resolvedData) {
          updatedRa = resolvedData.ra;
          updatedDec = resolvedData.dec;
        }
        setResolvedSourceName(true);
      }
      const updateQueryParam =
        '/v1/data/get_product_types?ra=' +
        updatedRa +
        '&dec=' +
        updatedDec +
        '&radius=' +
        radius;
      console.log('Update Query param: ' + updateQueryParam);
      setQueryParam(updateQueryParam);

      const response = await saveDataQuery({
        project_id: project?.id,
        name: name,
        query_param: updateQueryParam,
      });
      // Trigger the refresh of saved queries to get the latest one
      const updatedQueries = await fetchDataQueries(project?.id);
      setQueries(updatedQueries);
      handleClose();
      console.log('Response ' + response);
    } catch (error: any) {
      console.log('Error occured ' + error.message);
      setLoading(false);
    } finally {
      setLoading(false);
    }
  };

  const modalStyle = {
    backgroundColor: theme === 'dark' ? '#252858' : '#fff',
    color: theme === 'dark' ? '#fff' : '#000',
    borderRadius: '0px',
    border: theme === 'dark' ? '1px solid #252858' : '1px solid #ddd',
  };

  const toTitleCase = (str: string | undefined) => {
    return str?.toLowerCase().replace(/\b\w/g, (char) => char.toUpperCase());
  };

  return (
    <Modal show={show} onHide={handleClose} centered>
      <div className="custom-modal modal-content" style={modalStyle}>
        <Modal.Header closeButton className="modal-header">
          <Modal.Title>Save Data Query</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body">
          <Form>
            <Form.Group controlId="queryName">
              <Form.Label>Search Query Name</Form.Label>
              <Form.Control
                className="input-field-source-name"
                style={{
                  marginBottom: '10px',
                  borderRadius: '0px',
                  outline: 'none',
                  boxShadow: 'none',
                  borderColor: '#ccc',
                }}
                type="text"
                placeholder={t('Enter query name')}
                value={name}
                autoComplete="off"
                maxLength={20}
                onChange={handleInputChange}
              />
              {error && (
                <p style={{ color: 'red', fontSize: '12px', marginTop: '5px' }}>
                  {error}
                </p>
              )}
            </Form.Group>

            <Form.Group controlId="queryParam">
              <Form.Label>Search Query Parameters</Form.Label>
              {/* <p className="text-muted" style={{fontSize: '16px' }}>
              { queryParam }
            </p> */}
              <Form.Control
                type="text"
                className="text-muted"
                style={{
                  marginBottom: '20px',
                  textAlign: 'left',
                  borderRadius: '0px',
                  border: '0px',
                  outline: 'none',
                  boxShadow: 'none',
                  borderColor: '#ccc',
                }}
                value={queryParam}
                readOnly
              />
            </Form.Group>
          </Form>
          <span className="text-muted" style={{ fontSize: '14px', fontStyle: 'italic' }}>
            Note: This search query will be saved under{' '}
            <b>{toTitleCase(project?.name)}</b> project.
          </span>
        </Modal.Body>
        <Modal.Footer>
          <ToolButton
            key={t('Close')}
            name="close"
            onClick={handleClose}
            tooltip="Close save query modal"
            text={t('Close')}
            type={ToolButtonType.SECONDARY}
          />
          {/* disabled={saveQueryMutation.isLoading}> */}
          {/* {saveQueryMutation.isLoading ? "Saving..." : "Save"} */}
          <ToolButton
            key={t('Save')}
            name="save"
            onClick={handleSave}
            disabled={loading}
            tooltip="Save query"
            text={loading ? t('Saving...') : t('Save')}
            type={ToolButtonType.PRIMARY}
          />
        </Modal.Footer>
      </div>
    </Modal>
  );
};

export default SaveQueryModal;

export enum DataManagementPage {
  DataManagement,
  Collections,
  Namespaces,
  Items,
  Dataset,
  JobDetails,
  Collection,
} // DataManagementPage

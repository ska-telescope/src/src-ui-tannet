import React from 'react';
import '../../App.css';
import './user-control.css';
import CSS from 'csstype';

// images
import tickIcon from '../../icons/tick.32.png';
import cancelIcon from '../../icons/disconnect.32.png';

// classes

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

interface UserControlLoginBoxProps {
  displayed: boolean;
  buttonHandler: any;
} // UserControlLoginBoxProps

interface UserControlLoginBoxState {} // UserControlLoginBoxState

//	--------------------------------------------------------------------------
//
//	H T M L   C U S T O M   C O M P O N E N T S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	C L A S S   D E F I N I T I O N
//
//	--------------------------------------------------------------------------

export default class UserControlLoginBox extends React.Component<
  UserControlLoginBoxProps,
  UserControlLoginBoxState
> {
  constructor(props: UserControlLoginBoxProps) {
    super(props);
  }

  //	------------------------------------------------------------
  //
  //	Component code
  //
  //	------------------------------------------------------------

  render() {
    const loginPopup: CSS.Properties = {
      display: this.props.displayed === true ? 'block' : 'none',
      position: 'fixed',
      top: '104px',
      right: '4px',
      border: 'none',
      zIndex: '9',
    };

    return (
      <div style={loginPopup}>
        <div className="login-box">
          <div className="login-title">Login to SRC-NET prototype</div>
          <div className="login-item-container">
            <div className="login-label">User name:</div>
            <input
              id="txtUsername"
              type="text"
              className="login-textbox"
              placeholder="user"
            />
          </div>
          <div className="login-item-container">
            <div className="login-label">Password:</div>
            <input
              id="txtPassword"
              type="password"
              className="login-textbox"
              placeholder="password"
            />
          </div>
          <div className="login-button-container">
            <button
              name="btnConnect"
              className="login-button-left"
              onClick={this.props.buttonHandler}
            >
              <div className="login-button-image">
                <img id="icnConnect" src={tickIcon} alt="" width="30" height="30" />
              </div>
              <div className="login-button-text">Connect</div>
            </button>
            <button
              name="btnCancel"
              className="login-button-right"
              onClick={this.props.buttonHandler}
            >
              <div className="login-button-image">
                <img id="icnCancel" src={cancelIcon} alt="" width="30" height="30" />
              </div>
              <div className="login-button-text">Cancel</div>
            </button>
          </div>
        </div>
      </div>
    );
  }
} // UserControlLoginBox

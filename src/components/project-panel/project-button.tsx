import React from 'react';
import '../../App.css';
import CSS from 'csstype';

// images

// classes

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	H T M L   C U S T O M   C O M P O N E N T S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	C L A S S   D E F I N I T I O N
//
//	--------------------------------------------------------------------------

export default function ProjectButton(props: {
  name: string;
  icon: string;
  projectName: string;
  description: string;
  onClick: any;
  selected?: boolean;
}) {
  return (
    <div
      id={props.name}
      title={props.description}
      style={{
        flex: '0 0 70px',
        width: '86px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '2px 2px 2px 2px',
        borderRadius: '6px',
        backgroundColor: 'transparent',
        cursor: 'pointer',
        border: 'none',
        //border: '1pt solid white'
      }}
      onClick={props.onClick}
    >
      <div
        style={{
          flex: '0 0 32px',
          width: '32px',
          display: 'flex',
          flexDirection: 'row',
          textAlign: 'center',
          alignItems: 'center',
          height: '100%',
          margin: '2px 2px 2px 2px',
        }}
      >
        {props.icon !== '' ? (
          <img
            style={{ opacity: props.selected === true ? '1' : '0.3', userSelect: 'none' }}
            src={props.icon}
            alt=""
            width="32"
            height="32"
          />
        ) : (
          <div
            style={{
              flex: '0 0 32px',
              height: '32px',
              backgroundColor: 'black',
              borderRadius: '10px',
              color: props.selected ? 'white' : 'gray',
              fontSize: '17pt',
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column',
              userSelect: 'none',
            }}
          >
            {props.projectName.slice(0, 1)}
          </div>
        )}
      </div>
      <div
        style={{
          flex: '1 1 0px',
          height: '100%',
          display: 'flex',
          textAlign: 'center',
          flexDirection: 'row',
          alignItems: 'center',
          fontSize: '0.8em',
        }}
      >
        <div
          style={{
            width: '100%',
            fontSize: '1.1em',
            /*fontWeight: props.selected === true ? '800' : '400',*/
            color: props.selected === true ? 'white' : '#cccccc',
            fontFamily: "'Noto Sans', Verdana, Geneva, Tahoma, sans-serif",
            userSelect: 'none',
          }}
        >
          {props.projectName}
        </div>
      </div>
    </div>
  );
} // ProjectButton

import { rest } from 'msw';
import { APIPrefix } from '../utils/functions';

export const handlers = [
  rest.get(APIPrefix() + '/v1/get_service_status', (req, res, ctx) => {
    const service = req.url.searchParams.get('service');
    // const url = new URL(request.url);
    // const service = url.searchParams.get('service');

    // Mock response based on each service
    let mockStatus = 'UNKNOWN';
    if (service === 'iam') mockStatus = 'RUNNING';
    if (service === 'rucio') mockStatus = 'STOPPED';

    return res(
      ctx.status(200),
      ctx.json({
        service_id: service,
        service_status: mockStatus,
      }),
    );
  }),
  rest.get(APIPrefix() + '/v1/data/get_product_types', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        result: [
          {
            dataproduct_type: 'cube',
            num_records: 7003,
          },
          {
            dataproduct_type: 'image',
            num_records: 2803,
          },
          {
            dataproduct_type: 'test_file',
            num_records: 95,
          },
          {
            dataproduct_type: '',
            num_records: 134,
          },
        ],
      }),
    );
  }),
  rest.get(APIPrefix() + '/v1/resolve', (req, res, ctx) => {
    const sourceName = req.url.searchParams.get('source_name');
    let mockResponse = {};
    if (sourceName) {
      mockResponse = {
        ra: '200',
        dec: '-85',
        description: 'Resolved by SIMBAD: M 51, Object Type: Sy2',
      };
    }
    return res(ctx.status(200), ctx.json(mockResponse));
  }),
  rest.get(APIPrefix() + '/v1/site_capabilities/list_sites', (req, res, ctx) => {
    let mockResponse = {
      sites: ['AUSSRC', 'CANSRC', 'CHSRC', 'CNSRC', 'SKAOSRC', 'SWESRC', 'UKSRC'],
    };
    return res(ctx.status(200), ctx.json(mockResponse));
  }),
  rest.get(APIPrefix() + '/v1/site_capabilities/list_service_types', (req, res, ctx) => {
    let mockResponse = {
      services: [
        'canfar',
        'echo',
        'gatekeeper',
        'ingest',
        'jupyterhub',
        'monitoring',
        'perfsonar',
        'prepare_data',
        'soda_async',
        'soda_sync',
      ],
    };
    return res(ctx.status(200), ctx.json(mockResponse));
  }),
  rest.get(APIPrefix() + '/v1/site_capabilities/list_compute', (req, res, ctx) => {
    const param = req.url.searchParams.get('');
    let mockResponse = {
      compute: [
        {
          site: 'SWESRC',
          compute_id: '001',
          latitude: 57.6898,
          longitude: 11.9742,
          hardware_capabilities: [],
          hardware_type: ['container'],
          description:
            'Resources on our k8s-cluster running in an OpenStack environemnt.',
          middleware_version: '',
          associated_services: [
            {
              id: '001',
              type: 'echo',
              prefix: 'https://',
              host: 'gatekeeper.swesrc.chalmers.se',
              path: '/gatekeeper-echo',
              enabled: true,
              other_attributes: {},
              is_mandatory: false,
              is_proxied: false,
            },
          ],
        },
      ],
    };
    return res(ctx.status(200), ctx.json(mockResponse));
  }),
  rest.get(APIPrefix() + '/v1/get_user_tokens', (req, res, ctx) => {
    let mockResponse = {
      user_tokens: [],
    };
    return res(ctx.status(200), ctx.json(mockResponse));
  }),

  rest.get(APIPrefix() + '/v1/fetch_data_query', (req, res, ctx) => {
    const project = req.url.searchParams.get('project');
    let mockResponse = {
      status: 'success',
      data_queries: [],
    };
    return res(ctx.status(200), ctx.json(mockResponse));
  }),
];

import { StylesConfig } from 'react-select';

// set style of Select box (dark colour scheme).
export const customStylesDark: StylesConfig = {
  control: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    height: 36,
    border: state.isDisabled === true ? '1px solid gray' : '1pt solid white',
    background: 'transparent',
    borderRadius: '0px',
    '&:hover': {
      border: '1px solid white',
    },
    //boxShadow: state.isFocused ? "0px 0px 6px #ff8b67" : "none",
    // "&": {
    //   border: "1px solid #cccccc",
    //   boxShadow: "none"
    // },
    //"&:focus":	{
    //		border: "1px solid #ff8b67",
    //		boxShadow: "0px 0px 6px #ff8b67"
    //		},
    // "&:active": {
    //   border: "1px solid #ff8b67",
    //   boxShadow: "0px 0px 6px #ff8b67"
    // }
  }),
  singleValue: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: state.isDisabled === true ? 'gray' : 'white',
  }),
  placeholder: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: '#909090',
  }),
  input: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: state.isDisabled === true ? 'gray' : 'white',
  }),
  menu: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    backgroundColor: '#454545',
    borderRadius: '10px',
    border: 'solid 1pt black',
  }),
  dropdownIndicator: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: state.isDisabled === true ? 'gray' : 'white',
  }),
  clearIndicator: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: state.isDisabled === true ? 'gray' : 'white',
  }),
  option: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: 'white',
    borderRadius: '5px',
    '&:hover': {
      backgroundColor: '#0d64bf',
      transition: '0.3s',
      color: 'white',
    },
    backgroundColor: 'transparent',
  }),
};

// set style of Select box (light colour scheme).
export const customStylesLight: StylesConfig = {
  control: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    border: state.isDisabled === true ? '1px solid gray' : '1pt solid black',
    background: 'transparent',
    borderRadius: '0px',
    '&:hover': {
      border: '1px solid black',
    },
  }),
  singleValue: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: state.isDisabled === true ? 'gray' : 'black',
  }),
  placeholder: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: '#707070',
  }),
  input: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: 'black',
  }),
  menu: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    backgroundColor: 'white',
    borderRadius: '10px',
    border: 'solid 1pt black',
  }),
  dropdownIndicator: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: state.isDisabled === true ? 'gray' : 'black',
  }),
  clearIndicator: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: state.isDisabled === true ? 'gray' : 'black',
  }),
  option: (provided: Record<string, unknown>, state: any) => ({
    ...provided,
    color: 'black',
    borderRadius: '5px',
    '&:hover': {
      backgroundColor: '#0d64bf',
      transition: '0.3s',
      color: 'white',
    },
    backgroundColor: 'transparent',
  }),
};

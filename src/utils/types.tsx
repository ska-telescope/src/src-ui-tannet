export interface AccessToken {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  id_token: string;
  expires_at: number;
} // AccessToken

//	------------------------------------------------------------
//
//	the currently selected tool from the home page.
//
//	------------------------------------------------------------

export enum ToolType {
  HOME,
  SEARCH_CATALOG,
  SEARCH_COMPUTE,
  DATA_MANAGEMENT,
  NOTEBOOK,
  VISUALISE_DATA,
  USER_MANAGEMENT,
  WORKFLOW,
  MY_FILES,
  TERMINAL,
  VIRTUAL_MACHINE,
  OIDC_TOKENS,
  VIEW_NOTIFICATIONS,
} // ToolType

//	------------------------------------------------------------
//
//	hold details on a container, dataset, or file.
//
//	------------------------------------------------------------

export interface JobItemType {
  scope: string;
  name: string;
  type: string;
  bytes: number;
  adler32: string;
  md5: string;
} // JobItemType

//	------------------------------------------------------------
//
//	hold details on data-management jobs.
//
//	------------------------------------------------------------

export interface JobType {
  row_id: number;
  job_id: string;
  to_storage_area_uuid: string;
  to_site: string;
  to_storage_identifier: string;
  status: string;
  parent_namespace: string;
  parent_name: string;
  containers: JobItemType[];
  datasets: JobItemType[];
  files: JobItemType[];
  num_items: number;
  expires_at: string;
  expires_in: string;
} // JobType

//	------------------------------------------------------------
//
//	hold details on notifications.
//
//	------------------------------------------------------------

export interface NotificationType {
  notificationID: number;
  notificationType: string;
  notificationText: string;
  readFlag: boolean;
  additionalInfo: any;
  createdAt: string;
} // NotificationType

//	------------------------------------------------------------
//
//	hold details on the storage locations for each site
//
//	------------------------------------------------------------

export interface SiteStorageAreas {
  site: string;
  storage_areas: {
    associated_storage_id: string;
    storage_id: string;
    storage_type: string;
    relative_path: string;
    identifier: string;
  }[];
} // SiteStorageAreas

//	------------------------------------------------------------
//
//	hold details on local JupyterHUB services for each site
//
//	------------------------------------------------------------

export interface JupyterHUBs {
  site: string;
  associated_services: {
    id: string;
    prefix: string;
    host: string;
    path: string;
    identifier: string;
    port: number;
  }[];
} // JupyterHUBs

//	------------------------------------------------------------
//
//	hold details for single data query
//
//	------------------------------------------------------------
export interface ResolveName {
  ra: number;
  dec: number;
  description: string;
}

//	------------------------------------------------------------
//
//	hold details for single data query
//
//	------------------------------------------------------------
export interface DataQuery {
  data_query_id: number;
  project: number;
  name: string;
  data_query: string;
  last_updated_at: string;
}
//	------------------------------------------------------------
//
//	hold details for data queries
//
//	------------------------------------------------------------
export interface APIResponse<T> {
  status: string;
  message: string;
  data_queries: T[];
}
//	------------------------------------------------------------
//
//	For saving a new data query
//
//	------------------------------------------------------------
export interface SaveQueryRequest {
  project_id: number | undefined;
  name: string;
  query_param: string;
}

export interface SaveQueryResponse {
  status: string;
  message: string;
}

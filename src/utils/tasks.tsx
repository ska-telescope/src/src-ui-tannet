//	--------------------------------------------------------------------------
//
//	C O N S T A N T S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	T Y P E S
//
//	--------------------------------------------------------------------------

//	------------------------------------------------------------
//
//	tasks that require access tokens are stored in a data structure,
//	so that if the tokens need to be refreshed the tasks can
//	be completed using the refreshed tokens.
//
//	------------------------------------------------------------

export enum TaskType {
  NONE,
  UPDATE_SETTINGS,
  LOAD_SETTINGS,
  UPDATE_USER_SERVICE_TOKEN,
  MOVE_DATA_TO_STORAGE,
  GET_DATA_MANAGEMENT_JOBS,
  DELETE_NOTIFICATIONS,
  MARK_NOTIFICATIONS_READ,
  CREATE_NEW_DATA_COLLECTION,
  ADD_TO_EXISTING_DATA_COLLECTION,
  REMOVE_FROM_DATA_COLLECTION,
  DELETE_DATA_COLLECTION,
} // TaskType

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

//	------------------------------------------------------------
//
//	store details of the current task
//
//	the parameters object can be any type defined within this
//	file, or a standard type. i.e. if the task type is
//	MOVE_DATA_TO_STORAGE, then the parameters object will be of
//	type MoveDataToStorage.
//
//	not all task types have associated parameters. the following
//	list of task types that require parameters should be
//	maintained:
//
//	TaskType				Parameter type
//	--------               		--------------
//
//	MOVE_DATA_TO_STORAGE:			MoveDataToStorage
//	UPDATE_SETTINGS:			UserSettings
//	UPDATE_USER_SERVICE_TOKEN:		UserServiceToken
//	DELETE_NOTIFICATIONS:			number[]
//	MARK_NOTIFICATIONS_READ:		number[]
//	CREATE_NEW_DATA_COLLECTION:		DataCollection
//	ADD_TO_EXISTING_DATA_COLLECTION:	DataCollection
//	REMOVE_FROM_DATA_COLLECTION:		DataCollection
//	DELETE_DATA_COLLECTION:		DataCollection
//
//	------------------------------------------------------------

export interface CurrentTask {
  taskType: TaskType;
  parameters?: any;
} // CurrentTask

//	------------------------------------------------------------
//
//	parameters for creating or amending the name of a
//	data collection.
//
//	------------------------------------------------------------

export interface DataCollection {
  dataCollection?: { id: number; name: string };
  project?: { id: number; name: string };
  dids?: {
    namespace: string;
    filename: string;
  }[];
  collectionIDs?: number[];
  messageSearchCatalog?: boolean;
} // DataCollection

//	------------------------------------------------------------
//
//	parameters for launching a data-management job, moving
//	data to a storage location.
//
//	------------------------------------------------------------

export interface MoveDataToStorage {
  toStorageAreaUUID: string;
  toSite: string;
  toStorageIdentifier: string;
  lifetime: number;
  filesToMove: {
    namespace: string;
    name: string;
    filetype: string;
    bytes: number;
  }[];
} // MoveDataToStorage

//	--------------------------------------------------------------------------
//
//	update user settings
//
//	--------------------------------------------------------------------------

export interface UserSettings {
  darkMode?: boolean;
  language?: string;
} // UserSettings

//	--------------------------------------------------------------------------
//
//	update a user service token
//
//	--------------------------------------------------------------------------

export interface UserServiceToken {
  serviceID: string;
  usingToken: boolean;
  username: string;
  userToken: string;
} // UserServiceToken

// types
import { ToolType } from './types';
import { DataManagementPage } from '../components/data-management/types';

//	--------------------------------------------------------------------------
//
//	C O N S T A N T S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	T Y P E S
//
//	--------------------------------------------------------------------------

//	------------------------------------------------------------
//
//	when events are raised we set the event target. Global
//	events are propogated to ALL components, and other events
//	are propogated only to the components at which they are
//	targetted.
//
//	------------------------------------------------------------

export enum EventTarget {
  GLOBAL,
  APP,
  DATA_MANAGEMENT_TABLE,
  SEARCH_CATALOG,
  SEARCH_CATALOG_FORM,
  SEARCH_CATALOG_DATA_COLLECTION_PANEL,
  SEARCH_COMPUTE_TABLE,
  VIEW_NOTIFICATIONS,
  USER_CONTROL_DROPDOWN_MENU,
} // EventTarget

//	------------------------------------------------------------
//
//	all the possible event types are listed here
//
//	------------------------------------------------------------

export enum EventType {
  DATA_COLLECTION_ADDED,
  DATA_COLLECTION_DELETED,
  DATA_COLLECTION_ALREADY_EXISTS,
  DATA_COLLECTION_DOES_NOT_EXIST,
  USER_NOT_PROJECT_MEMBER_NEW_COLLECTION,
  USER_NOT_PROJECT_MEMBER_EXISTING_COLLECTION,
  ITEMS_ADDED_TO_DATA_COLLECTION,
  ITEMS_REMOVED_FROM_DATA_COLLECTION,
  INITIATE_DM_SEARCH,
  INITIATE_COMPUTE_SEARCH,
  INITIATE_DATA_SEARCH,
  UPDATE_SEARCH_RA_DEC,
  LAUNCH_NOTEBOOK,
  DELETE_NOTIFICATIONS,
  MARK_NOTIFICATIONS_READ,
  REFRESH_NOTIFICATIONS,
} // EventType

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

//	------------------------------------------------------------
//
//	holds details on a current event.
//
//	------------------------------------------------------------

export interface Event {
  eventTarget: EventTarget;
  eventType: EventType;
  parameters?: any;
} // Event

//	------------------------------------------------------------
//
//	raise an event to initiate a data-management search.
//
//	------------------------------------------------------------

export interface InitiateDMSearchParams {
  namespace?: string;
  filename?: string;
  jobID?: string;
  dataset?: string;
  project?: {
    id: number;
    name: string;
  };
  collection?: {
    id: number;
    name: string;
  };
  fileType?: string;
  pageDisplayed: DataManagementPage;
  showPage?: boolean;
} // InitiateDMSearchParams

//	------------------------------------------------------------
//
//	raise an event following an attempt to add a collection or
//	add items to a collection.
//
//	------------------------------------------------------------

export interface AddCollectionEvent {
  collectionName?: string;
  projectName?: string;
  itemsAdded?: number;
  errorTarget?: EventTarget;
} // AddCollectionEvent

//	------------------------------------------------------------
//
//	raise an event to initiate a compute search.
//
//	------------------------------------------------------------

export interface InitiateComputeSearchParams {
  site?: string;
  gpu?: boolean;
  largeScratch?: boolean;
  highMemory?: boolean;
  fastScratch?: boolean;
  description?: string;
  hardwareType?: string;
  middlewareVersion?: string;
  serviceType?: string;
  latitude?: number;
  longitude?: number;
  showPage?: boolean;
} // InitiateComputeSearchParams

//	------------------------------------------------------------
//
//	a position that also holds a string identifier of an
//	observation.
//
//	------------------------------------------------------------

export interface PositionWithID {
  position: {
    id: string;
    ra: number;
    dec: number;
  }[];
} // PositionWithID

//	------------------------------------------------------------
//
//	a position and fov.
//
//	------------------------------------------------------------

export interface PositionAndFOV {
  position?: {
    ra: number;
    dec: number;
  };
  fov?: number;
} // PositionAndFOV

import React, { useState, useEffect, useRef } from 'react';
import '../App.css';
import CSS from 'csstype';

// images
import infoIcon from '../icons/info.512.png';
import errorIcon from '../icons/error.512.png';

// classes

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

//	------------------------------------------------------------
//
//	the type of popup message.
//
//	------------------------------------------------------------

export enum MessageType {
  INFO,
  ERROR,
} // MessageType

//	--------------------------------------------------------------------------
//
//	H T M L   C U S T O M   C O M P O N E N T S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	C L A S S   D E F I N I T I O N
//
//	--------------------------------------------------------------------------

//	-------------------------------------------------
//
//	Popup message.
//
//	Parameters:
//
//		nearComponent - the id of a component
//			that we need to be near
//		posOffset - the offset from the component
//			position.
//		setSetMessage - used to set the event
//			function.
//		setErrorState - used to set the state of
//			(for example) an input box to
//			error while the message is
//			displayed.
//
//	-------------------------------------------------

export function PopupMessage(props: {
  nearComponent: string;
  posOffset: {
    left: number;
    top: number;
  };
  setSetMessage: any;
  setErrorState?: any;
}) {
  //	--------------------------------------------------------------------------
  //
  //	C O N S T A N T S
  //
  //	--------------------------------------------------------------------------

  const NOTIFICATION_GREEN = '#003500';
  const NOTIFICATION_RED = '#700000';

  //	--------------------------------------------------------------------------
  //
  //	D E C L A R A T I O N S
  //
  //	--------------------------------------------------------------------------

  const [sNotification, setNotification] = useState<string | undefined>(undefined);
  const [sSize, setSize] = useState<{ width: number; height: number }>({
    width: 285,
    height: 40,
  });
  const [sPos, setPos] = useState<{ left: number; top: number }>({ left: 80, top: 880 });
  const [sIcon, setIcon] = useState<any>(infoIcon);
  const [sColour, setColour] = useState<string>(NOTIFICATION_GREEN);

  const clearTimerRef = useRef<any>();

  //	--------------------------------------------------------------------------
  //
  //	F U N C T I O N S
  //
  //	--------------------------------------------------------------------------

  //	------------------------------------------------------------
  //
  //	closes a message when the timeout has arrived.
  //
  //	------------------------------------------------------------

  function closeMessage() {
    setNotification(undefined);
    if (props.setErrorState !== undefined) props.setErrorState(false);
  } // closeMessage

  //	------------------------------------------------------------
  //
  //	called when an external component wishes to display
  //	this popup.
  //
  //	------------------------------------------------------------

  function messageEvent(args: {
    messageText: string;
    messageType: MessageType;
    timeout: number;
  }) {
    // set position of element.
    var divComponent: any = document.querySelector(props.nearComponent);
    if (divComponent !== null) {
      setPos({
        left: divComponent.offsetLeft + props.posOffset.left,
        top: divComponent.offsetTop + props.posOffset.top,
      });
      console.log('pos:');
      console.log(props.nearComponent);
      console.log(
        divComponent.offsetLeft + props.posOffset.left,
        divComponent.offsetTop + props.posOffset.top,
      );
      console.log(props.posOffset);
    } else {
      setPos({ left: 0, top: 0 });
      console.log(0, 0);
    }

    // display the notification, and set the timeout.
    setNotification(args.messageText);
    switch (args.messageType) {
      case MessageType.INFO:
        setColour(NOTIFICATION_GREEN);
        setIcon(infoIcon);
        break;
      case MessageType.ERROR:
        setColour(NOTIFICATION_RED);
        setIcon(errorIcon);
        break;
    }

    // highlight the error field, if one is supplied.
    if (props.setErrorState !== undefined && args.messageType == MessageType.ERROR)
      props.setErrorState(true);

    clearTimeout(clearTimerRef.current);
    clearTimerRef.current = setTimeout(() => {
      closeMessage();
    }, args.timeout);
  } // messageEvent

  //	--------------------------------------------------------------------------
  //
  //	C O M P O N E N T S
  //
  //	--------------------------------------------------------------------------

  //	--------------------------------------------------------------------------
  //
  //	H O O K S
  //
  //	--------------------------------------------------------------------------

  // handle changes in size to the popup.
  const resizeNotificationBackground = React.useRef<ResizeObserver>(
    new ResizeObserver((entries: ResizeObserverEntry[]) => {
      // store the size of the popup in state.
      if (entries[0].contentRect.width > 0 && entries[0].contentRect.height > 0)
        setSize({
          width: entries[0].contentRect.width + 56,
          height: entries[0].contentRect.height + 20,
        });
      console.log('setting size:');
      console.log(entries[0].contentRect.width + 56, entries[0].contentRect.height + 20);
    }),
  );

  // create a reference to the HTML Div element that contains the popup, so that we can monitor for changes in size.
  const notificationPopup = React.useCallback(
    (container: HTMLDivElement) => {
      // check that this reference has actually been assigned to an element, and switch on observing.
      if (container !== null) resizeNotificationBackground.current.observe(container);
      // When element is unmounted, ref callback is called with a null argument
      // => best time to cleanup the observer
      else if (resizeNotificationBackground.current)
        resizeNotificationBackground.current.disconnect();
    },
    [resizeNotificationBackground.current],
  );

  //	------------------------------------------------------------
  //
  //	set the function that passes in notifications to this
  //	component.
  //
  //	------------------------------------------------------------

  useEffect(() => {
    console.log('SETTING POPUP MESSAGE EVENT');
    props.setSetMessage(messageEvent);
  }, []);

  //	------------------------------------------------------------
  //
  //	C O M P O N E N T   C O D E
  //
  //	------------------------------------------------------------

  return (
    <div>
      <div
        style={{
          width: sSize.width.toString() + 'px',
          height: sSize.height.toString() + 'px',
          backgroundColor: sColour,
          display: sNotification !== undefined ? 'inline-flex' : 'none',
          opacity: '0.9',
          position: 'fixed',
          left: sPos.left.toString() + 'px',
          top: sPos.top.toString() + 'px',
          border: 'none',
          zIndex: '1',
          borderRadius: '5px 5px 5px 0px',
        }}
      />
      <div
        style={{
          width: '20px',
          height: '15px',
          backgroundColor: sColour,
          display: sNotification !== undefined ? 'inline-flex' : 'none',
          opacity: '0.9',
          position: 'fixed',
          left: sPos.left.toString() + 'px',
          top: (sPos.top + sSize.height).toString() + 'px',
          border: 'none',
          zIndex: '1',
          borderRadius: '0px 0px 15px 0px',
        }}
      />
      <div
        data-type="INFOX"
        style={{
          width: sSize.width.toString() + 'px',
          height: sSize.height.toString() + 'px',
          display: sNotification !== undefined ? 'flex' : 'none',
          flexDirection: 'column',
          position: 'fixed',
          left: sPos.left.toString() + 'px',
          top: sPos.top.toString() + 'px',
          backgroundColor: 'transparent',
          border: 'none',
          zIndex: '1',
        }}
      >
        <div style={{ flex: '0 0 10px' }} />
        <div
          style={{
            flex: '0 0 auto',
            display: 'flex',
            flexDirection: 'row',
          }}
        >
          <div style={{ flex: '0 0 10px' }} />
          <div
            style={{
              flex: '0 0 26px',
              height: '26px',
            }}
          >
            <img src={sIcon} alt="" width="26" height="26" />
          </div>
          <div style={{ flex: '0 0 10px' }} />
          <div
            ref={notificationPopup}
            style={{
              flex: '0 0 auto',
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              color: 'white',
            }}
          >
            {sNotification !== undefined ? sNotification : '{undefined}'}
          </div>
          <div style={{ flex: '0 0 10px' }} />
        </div>
        <div style={{ flex: '0 0 10px' }} />
      </div>
    </div>
  );
} // PopupMessage

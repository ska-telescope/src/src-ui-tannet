import React, { useState } from 'react';
import '../App.css';
import CSS from 'csstype';

// images

// classes

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	H T M L   C U S T O M   C O M P O N E N T S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	C L A S S   D E F I N I T I O N
//
//	--------------------------------------------------------------------------

//	-------------------------------------------------
//
//	A scrollbox container.
//
//	Parameters:
//
//		direction: can be 'vertical' or 'horizontal'
//		flexDirection: 'row' or 'column'
//
//	-------------------------------------------------

export default function Scrollbox(props: {
  children: any;
  direction: string;
  flexDirection: any;
  flex: string;
  width?: string;
  height?: string;
  margin?: string;
}) {
  //	------------------------------------------------------------
  //
  //	C O M P O N E N T   C O D E
  //
  //	------------------------------------------------------------

  return (
    <div
      style={{
        ...(props.width !== undefined
          ? {
              width: props.width,
            }
          : {}),
        ...(props.height !== undefined
          ? {
              height: props.height,
            }
          : {}),
        ...{
          display: 'flex',
          flex: props.flex,
          flexDirection: props.direction === 'vertical' ? 'column' : 'row',
          margin: '0px',
          padding: '0px',
        },
      }}
    >
      <div
        style={{
          ...{
            flex: '1 1 auto',
          },
          ...(props.margin !== undefined
            ? {
                margin: props.margin,
              }
            : {}),
          ...(props.direction === 'vertical'
            ? {
                width: '100%',
                height: '0px',
              }
            : {
                width: '0px',
                height: '100%',
              }),
        }}
      >
        <div
          id="scrollbox"
          style={{
            ...{
              width: '100%',
              height: '100%',
              display: 'flex',
              flexDirection: props.flexDirection,
            },
            ...(props.direction === 'vertical'
              ? {
                  overflowY: 'auto',
                  overflowX: 'hidden',
                }
              : {
                  overflowX: 'auto',
                  overflowY: 'hidden',
                }),
          }}
        >
          {props.children}
        </div>
      </div>
    </div>
  );
} // Scrollbox

import React, { useState, useRef } from 'react';
import Select, { components, SelectInstance } from 'react-select';

//	--------------------------------------------------------------------------
//
//	T Y P E S
//
//	--------------------------------------------------------------------------

//	--------------------------------------------------------------------------
//
//	P R O P E R T I E S
//
//	--------------------------------------------------------------------------
interface EditableSelectProps {
  styles: any;
  isDisabled: boolean;
  options: any;
  placeholder: string;
  value: any;
  onChange: any;
  inputValue: string;
  onInputChange: any;
}
//	--------------------------------------------------------------------------
//
//	H T M L   C U S T O M   C O M P O N E N T S
//
//	--------------------------------------------------------------------------

// use a custom Input component to ensure the input is not hidden in a Select component.
const Input = (props: any) => <components.Input {...props} isHidden={false} />;

//	--------------------------------------------------------------------------
//
//	C L A S S   D E F I N I T I O N
//
//	--------------------------------------------------------------------------

export default function EditableSelect(props: EditableSelectProps) {
  //	--------------------------------------------------------------------------
  //
  //	D E C L A R A T I O N S
  //
  //	--------------------------------------------------------------------------

  // create reference to the Select component so we can set the focus.
  const selectRef = useRef<SelectInstance | null>(null);

  //	--------------------------------------------------------------------------
  //
  //	F U N C T I O N S
  //
  //	--------------------------------------------------------------------------

  const onFocus = () => {
    // ensure that the input component of the Select component does not lose focus when typing.
    if (props.value !== null) {
      selectRef.current?.focus();
    }
  };

  //	--------------------------------------------------------------------------
  //
  //	C O M P O N E N T S
  //
  //	--------------------------------------------------------------------------

  //	--------------------------------------------------------------------------
  //
  //	H O O K S
  //
  //	--------------------------------------------------------------------------

  //	--------------------------------------------------------------------------
  //
  //	C O M P O N E N T   C O D E
  //
  //	--------------------------------------------------------------------------

  return (
    <Select
      ref={selectRef}
      styles={props.styles}
      isDisabled={props.isDisabled}
      options={props.options}
      isClearable={true}
      value={props.value}
      inputValue={props.inputValue}
      placeholder={props.placeholder}
      onInputChange={props.onInputChange}
      onChange={props.onChange}
      onFocus={onFocus}
      controlShouldRenderValue={false}
      components={{ Input }}
    />
  );
} // EditableSelect()

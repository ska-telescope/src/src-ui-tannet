FROM node:lts
RUN mkdir -p /app
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build
RUN npm install -g serve
EXPOSE 3000
#CMD ["npm", "start"]
CMD serve -s build
